declare module 'proschet' {
	export default function proschet(nounForms: [string, string, string]) : (c: number) => string;
}