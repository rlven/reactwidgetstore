declare module 'get-random-values' {
	export default function getRandomValues(array: Uint8Array): Uint8Array;
}