import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Widget} from './ReactComponents/Widget/Widget';
import './App.scss';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

ReactDOM.render(<Widget/>, document.getElementById('react-root'));