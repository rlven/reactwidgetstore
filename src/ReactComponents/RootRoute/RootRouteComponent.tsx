import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import '../../App.scss';

export class RootRouteComponent extends React.Component {

    render() {
    return <BrowserRouter>
        <Switch>
            <Route exact path='/' >
                <div>helo world</div>
            </Route>
        </Switch>
    </BrowserRouter>;
    }
}