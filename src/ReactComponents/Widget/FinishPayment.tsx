import React from 'react';
import WidgetStore from './WidgetStore';
import { observer } from 'mobx-react';
import {PreloaderComp} from './Preloader';

type Props = {
    store: WidgetStore
};

@observer
class FinishPaymentComponent extends React.Component<Props>{
    
    render(){
        const store = this.props.store;
        if(!store.IsPaymentFinish){
            return <PreloaderComp/>
        }

        return <div>
                <h1>Success!!!</h1>
            </div>
    }
}

export const FinishPayment = FinishPaymentComponent;

//#region styled-components


//#endregion