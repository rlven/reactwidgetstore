import React from 'react';
import styled from 'styled-components';
import {Company} from './CompanyTitle';
import {FormArea} from './FormArea';
import WidgetStore from './WidgetStore';
import Map from './MapAutocomplete';
import { observer } from 'mobx-react'; 
import {FleetPage} from './FleetChoosePage';
import {OrderPreview} from './OrderPreview';
import {Payment} from './Payment';
import {FinishPayment} from './FinishPayment';
import {PreloaderComp} from './Preloader';

type Props =  {
    store: WidgetStore
};

@observer
export default class Wizzard extends React.Component<Props> {

    render() {
        const store = this.props.store;
        
        if(store.CompanyInfo===null){
            return <PreloaderComp/>;
        }

        const firstWizzard =                         
        <MainContainer>
            <Company
                store={store}/>
            <MapContainer>
                <Map
                store={store}/>
            </MapContainer>
            <FormArea
                store={store}/>
        </MainContainer>

        const secondWizzard = 
        <WizzardContainer>
            <Company
                store={store}/>
            <FleetPage
                store={store}/>
        </WizzardContainer>

        const thirdWizzard = 
        <WizzardContainer>
            <Company
                store={store}/>
            <OrderPreview
                store={store}/>
        </WizzardContainer>

        const forthWizzard = 
        <WizzardContainer>
            <Payment
                store={store}/>
        </WizzardContainer>

        const fifthWizzard =
        <WizzardContainer>
            <FinishPayment
                store={store}/>
        </WizzardContainer>

        let renderingWizzard = firstWizzard;

        switch (store.WizzardCurrentPage) {
            case 2:
                renderingWizzard = secondWizzard;
                break;
            case 3:
                renderingWizzard = thirdWizzard;
                break;
            case 4:
                renderingWizzard = forthWizzard;
                break;
            case 5:
                renderingWizzard = fifthWizzard;
            default:
                break;
        }

        return (<div>
            {renderingWizzard}
        </div>
        )
    }
}

//#region styled-components

const MainContainer = styled.div`
    height: 100%;
    width: 100%;
`;

const MapContainer = styled.div`
    height: 350px;
    width: 100%;
`;

const WizzardContainer = styled.div`
    height: 100%;
    width: 100%;
`;

//#endregion