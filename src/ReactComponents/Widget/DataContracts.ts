import {Car} from '../../Helpers/DataContracts';

export type BookInfo = {
    locationFromName: string
    locationFrom: Location,
    locationTo: Location,
    date: Date,
    peopleCount: number,
    airportTransfer: boolean,
    carTypeId: number,
    pickedCar: Car,
    orderNotes: string,
    totalDistance: number,
    amount: number
};

export type Location = {
    latitude: number,
    longitude: number
}

export type WidgetPosition = {
    x: number,
    y: number
}

export type WidgetColors ={
    TitleColor: string,
    BodyColor: string,
    SecondaryColor: string
}

export type ClientInfo = {
    firstName: string,
    lastName: string,
    phoneNumber: string,
    email: string
}