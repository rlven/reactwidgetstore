import React from 'react';
import styled from 'styled-components';
import WidgetStore from './WidgetStore';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { CloseWidgetIcon, BackArrowIcon } from '../../Items/icons';

type Props = {
    store: WidgetStore
};

@observer
class TermOfUseComponent extends React.Component<Props>{
    
    render(){
        const store = this.props.store;
        const companyInfo = toJS(store.CompanyInfo);
        return <Container>
                <CompanyInfoContainer style={{background: store.WidgetColors.TitleColor}}>
                {store.WizzardCurrentPage !== 1 &&
                <BackArrow onClick={store.closeTermOfUse}>
                    <BackArrowIcon/>
                </BackArrow>
                }
                <CompanyLogo>
                    <img className="company-logo-image" src={companyInfo?.companyLogoPath}/>
                </CompanyLogo>
                <CompanyName>
                    {companyInfo?.companyName != undefined ? companyInfo.companyName : "Company name"}
                </CompanyName>
                <CloseWidget onClick={store.closeWidget}>
                    <CloseWidgetIcon/>
                </CloseWidget>
            </CompanyInfoContainer>
            <TermsContainer>
                <TermsTitle>Term of use</TermsTitle>
                <p style={{color: "antiquewhite"}}>
                Checkout User Terms of Service — United States
                </p>
<p>Last updated: June 21, 2018</p>

<p>These Stripe Checkout User Terms of Service are a legal agreement between Stripe, Inc. (“Stripe”, “we” or “us”) and you, the user of the <a href="https://stripe.com/payments/checkout">Stripe Checkout</a> service (“you”). By using the Stripe Checkout service, you agree to be bound by these terms and conditions.</p>

<h6 style={{color: "antiquewhite"}}>1. General.</h6>
<p>The Stripe Checkout service (“Stripe Checkout”) is technology that makes it easier for merchants on the Internet (“Merchants”) to collect payment from individuals like you. Stripe Checkout also makes it easy for you to store a credit card or debit card (“Payment Credentials”) with Stripe for use across the websites of Merchants who’ve chosen to enable it.</p>

<h6 style={{color: "antiquewhite"}}>2. Using Stripe Checkout.</h6>
<p>When you check out on the website of a Merchant that has Stripe Checkout enabled, we will ask you if you’d like us to remember you. When you allow us to remember you, Stripe will store certain identifying information, such as a password, your email address, or your mobile phone number (“Stripe Credentials”), and your Payment Credentials. The advantage of remembering you is that it will make your checkout quicker and easier if you come back to the same website, or to any of the other websites that use Stripe (a “Checkout Enabled Site”) – this can be especially handy when you’re on a mobile device or don’t have your credit card in front of you.</p>

<p>If you elect to allow us to remember you, Stripe will use cookies to link your web browser to your Stripe Credentials and recognize when you return to a Checkout Enabled Site. If you come to a Checkout Enabled Site and we don’t recognize you (for example, because you’ve cleared your cookies, logged out, or you’re using a different device), we will provide a way for you to identify yourself and login via your Stripe Credentials (for example, by sending you a verification code via SMS text message). While you are logged in, Stripe will give you the ability to make purchases using your stored Payment Credentials. Stripe may also allow you to make a purchase with your Payment Credentials by sending a message directly from the email address or phone number stored as your Stripe Credentials (for example, to authorize a purchase via SMS).</p>

<p>If you send us text messages, or have us send you one, don’t forget that your carrier might charge you for that.</p>

<h6 style={{color: "antiquewhite"}}>3. Stripe’s Role.</h6>
<p>Stripe Checkout is a way of storing your Payment Credentials, but it doesn’t change anything else about your relationship with the Merchant you’re paying or your bank or credit card company. You are ultimately responsible for the purchases you make using Stripe Checkout. Also, the Merchant is the one responsible for providing you the goods or services that you purchase using Stripe Checkout, not Stripe. Stripe will use reasonable efforts to keep your Payment Credentials secure.</p>

<h6 style={{color: "antiquewhite"}}>4. Making Changes.</h6>
<p>If you want to delete your Payment Credentials, stop storing information using Stripe Checkout, or change your settings, please <a href="https://stripe.com/contact">contact us</a>.</p>

<h6 style={{color: "antiquewhite"}}>5. Representations and Warranties.</h6>
<p>By using Stripe Checkout you represent and warrant that you are at least 18 years of age and that you will not use Stripe Checkout for any fraudulent, unlawful or abusive purpose.</p>

<h6 style={{color: "antiquewhite"}}>6. Disclaimers.</h6>
<p>Stripe Checkout, including all content, software, functions, materials, and information made available on, provided in connection with or accessible through Stripe Checkout, are provided “as is.” To the fullest extent permissible by law, Stripe, its affiliates, and their agents, merchants or independent contractors (the “Disclaiming Entities”), make no representation or warranty of any kind whatsoever for the services or the content, materials, information and functions made accessible by Stripe Checkout, or for any breach of security associated with the transmission of sensitive information through Stripe Checkout. Each Disclaiming Entity disclaims without limitation, any warranty of any kind with respect to the services, noninfringement, merchantability, or fitness for a particular purpose. The Disclaiming Entities do not warrant that the functions contained in the services will be uninterrupted or error free. The Disclaiming Entities shall not be responsible for any service interruptions, including, but not limited to, system failures or other interruptions that may affect the receipt, processing, acceptance, completion or settlement of payment transactions. The Disclaiming Entities are not responsible for the accuracy of any payment instrument, offer, or reward program item information, including, without limitation, whether such information is accurate.</p>

<h6 style={{color: "antiquewhite"}}>7. Limitations of Liability; Force Majeure.</h6>
<p>In no event shall any Disclaiming Entity be responsible or liable to you or any third party under any circumstances for any indirect, consequential, special, punitive or exemplary, damages or losses, including but not limited to damages for loss of profits, goodwill, use, data, or other intangible losses which may be incurred in connection with any Disclaiming Entity or the services, or any goods, services, or information purchased, received, sold, or paid for by way of the services, regardless of the type of claim or the nature of the cause of action, even if the Disclaiming Entity has been advised of the possibility of such damage or loss. In no event shall the Disclaiming Entities’ total cumulative liability arising from or relating to these Terms of Service exceed $10 US dollars. Each party acknowledges that the other party has entered into these Terms of Service relying on the limitations of liability stated herein and that those limitations are an essential basis of the bargain between the parties. In addition to and without limiting any of the foregoing, no Disclaiming Entity shall have any liability for any failure or delay resulting from any condition beyond the reasonable control of such party, including but not limited to governmental action or acts of terrorism, earthquake, fire, flood or other acts of God, labor conditions, power failures and Internet disturbances.</p>

<h6 style={{color: "antiquewhite"}}>8. Governing Law.</h6>
<p>These Terms of Service will be governed by the laws of California, except for California’s choice of law rules, and by applicable federal United States laws. Each party agrees to submit to personal and exclusive jurisdiction of the courts located in San Francisco, California.</p>

<h6 style={{color: "antiquewhite"}}>9. Modification of Terms of Service; Notices.</h6>
<p>We have the right to change or add to these Terms of Service at any time, solely with prospective effect, and to change, delete, discontinue, or impose conditions on use of Stripe Checkout by posting such changes on our <a href="https://stripe.com/">website</a> or any other website we maintain or own. We may provide you with notice via email, postings on our <a href="https://stripe.com/">website</a>, or through other reasonable means. If you are an existing Stripe Checkout user, the changes will come into effect 10 days after we post the changes to our website, and your use of Stripe Checkout more than 10 days after we publish any such changes on our website, constitutes your acceptance of the modified Terms of Service. In the event that you do not agree with any such modification, your sole and exclusive remedy is to terminate your use of Stripe Checkout. You can access a copy of the current terms of these Terms of Service on our website at any time. You can find out when these Terms of Service were last changed by checking the “Last updated” date at the top of these Terms of Service.</p>

<h6 style={{color: "antiquewhite"}}>10. Assignment.</h6>
<p>You may not assign these Terms of Service or any rights or obligations hereunder, by operation of law or otherwise, without our prior written approval and any such attempted assignment shall be void. We reserve the right to freely assign these Terms of Service and the rights and obligations hereunder, to any third party without notice or consent. Subject to the foregoing, these Terms of Service shall be binding upon and inure to the benefit of the parties hereto, their successors and permitted assigns.</p>

<h6 style={{color: "antiquewhite"}}>11. Data.</h6>
<p>By using Stripe Checkout, you agree to the <a href="https://stripe.com/privacy">Stripe Privacy Policy</a>, which is incorporated into and forms part of these Terms of Service. You should be aware that your data may be transferred, processed and stored outside of your country (including, if you are located in the European Union, outside of the European Union), and that your data may be subject to disclosure as required by applicable law.</p>

<p>Stripe has implemented <a href="https://developers.google.com/recaptcha">reCAPTCHA</a> on Stripe Checkout. Use of reCAPTCHA is subject to the <a href="https://policies.google.com/privacy">Google Privacy Policy</a> and <a href="https://policies.google.com/terms">Terms of Service</a>.</p>

<h6 style={{color: "antiquewhite"}}>12. Survival.</h6>
<p>Upon termination of your use of Stripe Checkout or termination of these Terms of Service for any reason, in addition to this section, the following sections shall survive termination: Sections 5 through 13.</p>

<h6 style={{color: "antiquewhite"}}>13. Miscellaneous.</h6>
<p>Stripe’s failure to exercise or enforce any right or provision of the Terms of Service will not be considered a waiver of that right or provision. If any provision of these Terms of Service shall be adjudged by any court of competent jurisdiction to be unenforceable or invalid, that provision shall be limited or eliminated to the minimum extent necessary so that these Terms of Service shall otherwise remain in full force and effect and remain enforceable between the parties. Headings are for reference purposes only and in no way define, limit, construe or describe the scope or extent of such section. these Terms of Service, including Stripe’s policies governing Stripe Checkout referenced herein, constitutes the entire agreement between you and Stripe with respect to the use of Stripe Checkout. These Terms of Service are not intended and shall not be construed to create any rights or remedies in any parties other than you and Stripe, and no other person will have the ability to assert any rights as a third party beneficiary under these Terms of Service. These Terms of Service do not limit any rights that Stripe may have under trade secret, copyright, patent or other laws.</p>

            </TermsContainer>
            </Container>
    }
}

export const TermOfUse = TermOfUseComponent;

//#region styled-components


const Container = styled.div`
    color: white;
`; 

const TermsContainer = styled.div`
    margin: 0 auto;
    width: 80%;
    font-size: 18px;
`; 

const TermsTitle = styled.h4`
    margin-top: 20px;
    color: white;
`; 

const CloseWidget = styled.div`
    display: inline-block;
    float: right;
    margin-right: 10px;
    margin-top: 10%;
    @media (max-width: 500px) {
        margin-top: 12%;
  }
`;

const BackArrow = styled.div`
    display: inline-block;
    float: left;
    margin-left: 2%;
    height: 40px;
    width: 40px;
    margin-top: 10%;
    @media (max-width: 500px) {
        margin-top: 13%;
  }
`;

const CompanyInfoContainer = styled.div`
    height: 130px;
    border-bottom: 1px solid #C4C4C4;
    position: sticky;
    top: 0;
    z-index: 1;
    color: white;
`;

const CompanyLogo = styled.div`
    display: inline-block;
    border-radius: 50%;
    background: white;
    height: 75px;
    width: 75px;
    position: absolute;
    top:23%;
    left:23%;
    @media (max-width: 500px) {
        left: 9%;
  }
`;

const CompanyName = styled.div`
    display: inline-block;
    font-size: 24px;
    margin: 0;
    position: absolute;
    top: 34%;
    left: 42%;
    @media (max-width: 500px) {
        left: 34%;
  }
`;

//#endregion