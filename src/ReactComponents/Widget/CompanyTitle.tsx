import React from 'react';
import styled from 'styled-components';
import WidgetStore from './WidgetStore';
import { observer } from 'mobx-react';
import { CloseWidgetIcon, BackArrowIcon } from '../../Items/icons';
import { toJS } from 'mobx';
import {isMobile} from "react-device-detect";

type Props = {
    store: WidgetStore
};

@observer
class CompanyTitle extends React.Component<Props>{
    
    render(){
        const store = this.props.store;
        const companyInfo = toJS(store.CompanyInfo);

        return <CompanyInfoContainer style={{background: store.WidgetColors?.TitleColor}}>
        {store.WizzardCurrentPage !== 1 &&
        <BackArrow onClick={store.prevPage}>
            <BackArrowIcon/>
        </BackArrow>
        }
        <CompanyLogo>
            <img className="company-logo-image" src={companyInfo?.companyLogoPath}/>
        </CompanyLogo>
        <CompanyName>
            {companyInfo?.companyName != undefined ? companyInfo.companyName : "Company name"}
        </CompanyName>
        <CloseWidget onClick={isMobile ? store.closeModal : store.closeWidget}>
            <CloseWidgetIcon/>
        </CloseWidget>
    </CompanyInfoContainer>
    }
}

export const Company = CompanyTitle;

//#region styled-components

const CloseWidget = styled.div`
    display: inline-block;
    float: right;
    margin-right: 10px;
    margin-top: 10%;
    @media (max-width: 500px) {
        margin-top: 12%;
  }
`;

const BackArrow = styled.div`
    display: inline-block;
    float: left;
    margin-left: 2%;
    height: 40px;
    width: 40px;
    margin-top: 10%;
    @media (max-width: 500px) {
        margin-top: 13%;
  }
`;

const CompanyInfoContainer = styled.div`
    height: 130px;
    border-bottom: 1px solid #C4C4C4;
    position: sticky;
    top: 0;
    z-index: 1;
    color: white;
`;

const CompanyLogo = styled.div`
    display: inline-block;
    border-radius: 50%;
    background: white;
    height: 75px;
    width: 75px;
    position: absolute;
    top:23%;
    left:23%;
    @media (max-width: 500px) {
        left: 9%;
  }
`;

const CompanyName = styled.div`
    display: inline-block;
    font-size: 24px;
    margin: 0;
    position: absolute;
    top: 34%;
    left: 42%;
    @media (max-width: 500px) {
        left: 34%;
  }
`;

//#endregion