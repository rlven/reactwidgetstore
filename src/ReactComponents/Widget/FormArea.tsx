import React from 'react';
import styled from 'styled-components';
import { ClockIcon, PlaneIcon, MinusIcon, PlusIcon, HourIcon } from '../../Items/icons';
import WidgetStore from './WidgetStore';
import { observer } from 'mobx-react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {CarIcon} from './CarIcon';
import InputMask from 'react-input-mask';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import RequestHelper from '../../Helpers/RequestHelper';
import { toJS } from 'mobx';

type Props = {
    store: WidgetStore
};

@observer
class FormAreaComponent extends React.Component<Props>{
    store: WidgetStore;
    constructor(props:any){
        super(props);
        this.store = this.props.store;
    }

    componentDidMount(){
        RequestHelper.getAirlines()
        .then(result => {
            this.store.AirlinesList = result;
        });
        window.addEventListener('scroll', this.store.hideArrow, true);
    }
    render(){        
        const colors = toJS(this.store.WidgetColors);
        const carTypes = this.store.CompanyInfo?.carTypes.map((carType, i) => 
        
        this.store.checkCarType(carType.id) ? 
        <CarTypeContainer style={{backgroundImage: 'url("http://bookinglane-webui.us-east-2.elasticbeanstalk.com/images/suv.png")', background: colors.SecondaryColor}} 
                          onClick={()=>this.store.pickCarType(carType.id, carType.capacity, carType.name)} key={i}>
            <div className="cartype-picker">{carType.name}
                <CarIcon carType={carType.id}/>
            </div>
        </CarTypeContainer>
        :
        <CarTypeContainer onClick={()=>this.store.pickCarType(carType.id, carType.capacity, carType.name)} key={i}>
            <div className="cartype-picker">{carType.name}
                <CarIcon carType={carType.id}/>
            </div>
        </CarTypeContainer>);

        const additionalDestinations = [];

        for (var i = 1; i < this.store.LocationsCount; i++) {
            additionalDestinations.push(
            <div className="fake-destination-block"></div>);
          }

        return <div>
            {additionalDestinations}
            <FormAreaContainer>
                <InputTimeContainer>
                    <InputClockIcon>
                        <ClockIcon/>
                    </InputClockIcon>
                    <InputTimeField>
                        <input value={this.store.fullDate} id="booking-widget-time-inputName" onChange={this.store.handleDate} type="datetime-local" className="form-area-time-date-input"></input>   
                    </InputTimeField>
                </InputTimeContainer>
                <CirclesContainer>
                    <div className="booking-plus-circle-button" onClick={this.store.removePeople}>
                        <MinusIcon/>
                    </div>
                    <PeopleCountCircle id="boooking-widget-people-count">
                            <span className="people-count-text">{this.store.PeopleCount}</span><p className="people-count-text-paragraph">people</p>
                    </PeopleCountCircle>
                    <div className="booking-plus-circle-button" onClick={this.store.addPeople}>
                        <PlusIcon/>
                    </div>
                </CirclesContainer>
                <HourRadioContainer>
                    <HourIcon/>
                    <AirportTransferContent>
                        <HourlyText>
                            <span id="booking-widget-hourly-inputName">Hourly</span>
                        </HourlyText>
                        {this.store.IsHourly ?
                        <HourlyRadio>
                            <span>Yes</span>
                            <input defaultChecked name="booking-widget-hourly-inputName" className="airport-transfer-radio" type="radio"
                                    onClick={this.store.activateHourly}></input>
                            <span className="airport-transfer-yes-text">No</span>
                            <input name="booking-widget-hourly-inputName" className="airport-transfer-radio" type="radio" 
                                    onClick={this.store.deactivateHourly}></input>
                        </HourlyRadio>
                        :
                        <HourlyRadio>
                            <span>Yes</span>
                            <input name="booking-widget-hourly-inputName" className="airport-transfer-radio" type="radio" onChange={()=>{}}
                                    onClick={this.store.activateHourly}></input>
                            <span className="airport-transfer-yes-text">No</span>
                            <input defaultChecked onChange={()=>{}} name="booking-widget-hourly-inputName" className="airport-transfer-radio" type="radio" 
                                    onClick={this.store.deactivateHourly}></input>
                        </HourlyRadio>
                        }
                    </AirportTransferContent>
                </HourRadioContainer>
                <CirclesContainer>
                    <div className="booking-plus-circle-button" style={{pointerEvents: this.store.IsHourly? "auto" : "none"}} onClick={this.store.minusHour}>
                        <MinusIcon/>
                    </div>
                    <PeopleCountCircle id="booking-widget-hour-count-input">
                            <span className="people-count-text">{this.store.HoursCount}</span><p className="people-count-text-paragraph">hours</p>
                    </PeopleCountCircle>
                    <div className="booking-plus-circle-button" style={{pointerEvents: this.store.IsHourly? "auto" : "none"}} onClick={this.store.addHour}>
                        <PlusIcon/>
                    </div>
                </CirclesContainer>
                {!this.store.ArrowDissapear && 
                    <div id="bookinglane-form-area-down-arrow-id">
                        <div className="bookinglane-form-area-down-arrow">
                            <div className="arrow-down">
                                <span></span>
                            </div>
                        </div>
                        <div className="bookinglane-form-area-down-arrow-right">
                            <div className="arrow-right">
                                <span></span>
                            </div>
                        </div>
                        <div className="bookinglane-form-area-down-arrow-second">
                            <div className="arrow-down">
                                <span></span>
                            </div>
                        </div>   
                        <div className="bookinglane-form-area-down-arrow-right-second">
                            <div className="arrow-right">
                                <span></span>
                            </div>
                        </div>
                    </div>
                    }
                <AirportTransferContainer>
                    {/* <AirportTransferContent>
                        <AirportTransferText>
                            <span id="booking-widget-airportTransfer-inputName">Airport transfer</span>
                        </AirportTransferText>
                        {this.store.AirportTransferFormShow ?
                        <AirportTransferRadio>
                            <span>Yes</span>
                            <input defaultChecked name="booking-widget-airportTransfer-inputName" className="airport-transfer-radio" type="radio" 
                                onClick={this.store.showAirportTransferForm}></input>
                            <span className="airport-transfer-yes-text">No</span>
                            <input onChange={()=>{}} name="booking-widget-airportTransfer-inputName" className="airport-transfer-radio" type="radio" 
                                onClick={this.store.hideAirportTransferForm}></input>
                        </AirportTransferRadio>
                        :
                        <AirportTransferRadio>
                            <span>Yes</span>
                            <input onChange={()=>{}} name="booking-widget-airportTransfer-inputName" className="airport-transfer-radio" type="radio" 
                                onClick={this.store.showAirportTransferForm}></input>
                            <span className="airport-transfer-yes-text">No</span>
                            <input defaultChecked name="booking-widget-airportTransfer-inputName" className="airport-transfer-radio" type="radio" 
                                onClick={this.store.hideAirportTransferForm}></input>
                        </AirportTransferRadio>
                        }          
                    </AirportTransferContent> */}
                    {this.store.AirportTransferFormShow &&//render if store property equals true
                    <div>
                <AirportTransferContainerInput>
                    <PlaneIcon/>
                    <AirportTransferContent>
                    <AirportTransferDetailsContent>
                            <AirportTransferDetailsText id="booking-widget-airlines-info-input">
                                <InputAirportTransferField>
                                     <Autocomplete
                                        id="free-solo-demo"
                                        options={this.store.AirlinesList.map((options)=>options.name)}
                                        freeSolo
                                        style={{ width: "231px", color: "white" }}
                                        renderInput={(params) => <TextField {...params} onSelect={this.store.handleAirlinesInput} id="booking-widget-airportTransferField-inputName" className="forem-area-airlines-input" value={this.store.Airlines} placeholder="Airlines*" />}/>
                                </InputAirportTransferField>  
                            </AirportTransferDetailsText>
                        </AirportTransferDetailsContent>
                    </AirportTransferContent>
                </AirportTransferContainerInput>
                    {/* <AirportTransferIcon>
                        
                    </AirportTransferIcon> */}
                    <AirportTransferDetailsForm> 
                        {/* <AirportTransferDetailsContent>
                        <GateMeetingText>
                            <span id="booking-widget-airlines-gateMeeting-input">Airport direction</span>
                        </GateMeetingText>
                            {this.store.formData.airportTransferInfo.Direction ? 
                            <DirectionRadio>
                                <span>From</span>
                                <input defaultChecked onChange={()=>{}} onClick={this.store.aiportDirectionFrom} id="booking-widget-direction-inputName" name="booking-widget-direction-inputName" className="airport-transfer-radio" type="radio" ></input>
                                <span className="airport-transfer-direction-text">To </span>
                                <input onClick={this.store.aiportDirectionTo} id="booking-widget-direction-inputName" name="booking-widget-direction-inputName" type="radio"></input>
                            </DirectionRadio>
                            :
                            <DirectionRadio>
                                <span>From</span>
                                <input onClick={this.store.aiportDirectionFrom} id="booking-widget-direction-inputName" name="booking-widget-direction-inputName" className="airport-transfer-radio" type="radio" ></input>
                                <span className="airport-transfer-direction-text">To </span>
                                <input defaultChecked onChange={()=>{}} onClick={this.store.aiportDirectionTo} id="booking-widget-direction-inputName" name="booking-widget-direction-inputName" type="radio"></input>
                            </DirectionRadio>
                            }
                        </AirportTransferDetailsContent> */}
                        <AirportTransferDetailsContent>
                            <AirportTransferDetailsText id="booking-widget-airlines-flightNumber-input">
                                <InputLocationField>
                                    <InputMask value={this.store.FlightNumber} onChange={this.store.handleFlightNumberInput}id="booking-widget-airportLocation-inputName" type="" className="form-area-airport-transfer-input" placeholder="Flight number"></InputMask>
                                </InputLocationField>   
                            </AirportTransferDetailsText>
                        </AirportTransferDetailsContent>
                        <AirportTransferDetailsContent>
                        <GateMeetingText>
                            <span id="booking-widget-airlines-gateMeeting-input">Gate meeting</span>
                        </GateMeetingText>
                            {this.store.formData.airportTransferInfo.GateMeeting ? 
                            <GateMeetingRadio>
                                <span>Yes</span>
                                <input defaultChecked onChange={()=>{}} id="booking-widget-gateMeeting-inputName" name="booking-widget-gateMeeting-inputName" className="airport-transfer-radio" type="radio"
                                onClick={this.store.requireGateMeeting} ></input>
                                <span className="airport-transfer-yes-text">No </span>
                                <input onClick={this.store.noGateMeeting} id="booking-widget-gateMeeting-inputName" name="booking-widget-gateMeeting-inputName" type="radio"></input>
                            </GateMeetingRadio>
                            :
                            <GateMeetingRadio>
                                <span>Yes</span>
                                <input id="booking-widget-gateMeeting-inputName" name="booking-widget-gateMeeting-inputName" className="airport-transfer-radio" type="radio"
                                onClick={this.store.requireGateMeeting} ></input>
                                <span className="airport-transfer-yes-text">No </span>
                                <input defaultChecked onChange={()=>{}} onClick={this.store.noGateMeeting} id="booking-widget-gateMeeting-inputName" name="booking-widget-gateMeeting-inputName" type="radio"></input>
                            </GateMeetingRadio>
                            }
                        </AirportTransferDetailsContent>
                    </AirportTransferDetailsForm>
                    </div>
                    }
                    <AirportTransferPreference>
                        <AirportTransferIconHidden>
                            <PlaneIcon/>
                        </AirportTransferIconHidden>
                        <PreferenceContent>
                            <AirportTransferDetailsText>
                                <InputLocationField>
                                    <input id="booking-widget-preference-inputName" type="" className="form-area-airport-transfer-input" placeholder="Preference"></input>
                                </InputLocationField>
                            </AirportTransferDetailsText>
                        </PreferenceContent>
                    </AirportTransferPreference>
                </AirportTransferContainer>
                <CarsContainer id="booking-widget-cartype-inputName">
                    {carTypes}
                </CarsContainer>
                <ButtonContainer>
                    <button style={{background: colors.SecondaryColor}} className="next-button" onClick={this.store.submitForm}>NEXT</button>
                </ButtonContainer>
            </FormAreaContainer>
            </div>
    }
}

export const FormArea = FormAreaComponent;

//#region styled-components


const FormAreaContainer = styled.div`
    color: white;
    margin: 0 auto;
    width: 93%;
    margin-top: 40px;
`;

const InputLocationField = styled.div`
    display:inline-block;
    font-size: 14px;
    margin-left: 5px;
    width: 91.5%;
`;

const InputTimeContainer = styled.div`
    display:inline-block;
    margin-top: 10px;   
    width: 70%;
`;

const AirportTransferContainerInput = styled.div`
    display:inline-block;
    margin-top: 10px;   
    width: 100%;
`;

const HourRadioContainer = styled.div`
    display:inline-block;
    margin-top: 10px;   
    width: 70%;
    @media (max-width: 500px) {
        width: 92%;
  }
`;

const InputClockIcon = styled.div`
    display:inline-block;
    width: 5.6%;
    margin-top: 10px;
`;

const InputTimeField = styled.div`
    display:inline-block;
    width: 59%;
    margin-left: 2%;
    @media (max-width: 500px) {
        margin-left: 5%;
        width: 80%;
  }
`;

const CirclesContainer = styled.div`
    display:inline-block;
    font-family: IBM Plex Sans;
    font-style: normal;
    font-weight: normal;
    padding-top: 20px;
    margin-left: 10px;
`;

const PeopleCountCircle = styled.div`
    height: 50px;
    width: 50px;
    background-color: #3B3B40;
    border-radius: 50%;
    display: inline-block;
    margin-left: 8px;
    font-size: 10px;
    text-align: center;
    vertical-align: middle;
`;

const AirportTransferContainer = styled.div`
    margin-top: 20px;
`;

const AirportTransferIcon = styled.div`
    display:inline-block;
    margin-top: 10px;
`;

const AirportTransferIconHidden = styled.div`
    display:inline-block;
    margin-top: 10px;
    visibility: hidden;
`;


const AirportTransferDetailsText = styled.div`
    display:inline-block;
    font-family: IBM Plex Sans;
    font-size: 14px;
`;

const AirportTransferContent = styled.div`
    display:inline-block;
    width: 92%;
    padding-bottom: 5px;
`;

const PreferenceContent = styled.div`
    display:inline-block;
    width: 92%;
    padding-bottom: 5px;
    border-bottom: 1px solid rgba(255, 255, 255, 0.12);
`;

const AirportTransferDetailsContent = styled.div`
    display:inline-block;
    width: 55%;
    border-bottom: 1px solid rgba(255, 255, 255, 0.12);
    padding-bottom: 5px;
    padding-top: 15px;
    margin-left: 1.8%;
    color: white;
`;


const GateMeetingRadio = styled.div`
    display:inline-block;
    margin-left: 28.5%;
    line-height: 18px;
    @media (max-width: 500px) {
        margin-left: 3%;
        margin-top: 22px;
  }
`;
const HourlyRadio = styled.div`
    display:inline-block;
    margin-left: 35%;
    line-height: 18px;
    @media (max-width: 500px) {
        float: right;
        margin-left: 0%;
  }
`;

const AirportTransferDetailsForm = styled.div`
    margin-left: 20px;
`;

const HourlyText = styled.div`
    margin-left: 12px;
    display: inline-block;  
`;

const GateMeetingText = styled.div`
    display:inline-block;
    font-family: IBM Plex Sans;
    font-size: 14px;
    margin-left: 12px;
    @media (max-width: 500px) {
        display: block;
  }
`;

const InputAirportTransferField = styled.div`
    font-size: 14px;
    margin-left: 5px;
`;

const AirportTransferPreference = styled.div`
    margin-top: 30px;
`;

const CarsContainer = styled.div`
    margin-top: 30px;
    display: inline-flex;
`;

const CarTypeContainer = styled.div`
    display:inline-block;
    background: #292934;
    width: 23%;
    margin-left: 8px;
    height: 90px;
    border-radius: 10px;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    text-align: center;
    padding-top: 10px;
`;

const ButtonContainer = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 30px;
`;

const MinusIconContainer = styled.div`
    margin-top: -19%;
    @media (max-width: 500px) {
        margin-top: -15%;
  } 
`;

const PlusIconContainer = styled.div`
    margin-top: -19%;
    /* @media (max-width: 500px) {
        position: relative;
        top: -1%;
  } */
`;

//#endregion