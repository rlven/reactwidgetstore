import React from 'react';
import styled from 'styled-components';
import WidgetStore from './WidgetStore';
import { Accordion, Button, Modal } from 'react-bootstrap';
import {BookinglaneIcon} from '../../Items/icons';
import Wizzard from './Wizzard';
import Draggable from 'react-draggable';
import { observer } from 'mobx-react'; 
import {isMobile} from "react-device-detect";

type Props = {

};

@observer
class WidgetComponent extends React.Component<Props> {
    store: WidgetStore;

    constructor(props: Props) {
        super(props);
        this.store = new WidgetStore();
        this.store.extractKeyFromScrptSrc();
    }

    componentDidMount() {    
        this.store.fetchCompanyInfo();
    }

    render() {
        if(isMobile){
        var windowWidth = window.innerWidth;
        return (<div className="widget-button" id="react-bookinglane-widget-button-id">
            <button className="widget-modal-button" variant="primary" onClick={this.store.openModal}>
            <WidgetContainer>
                    <WidgetIconContainer>
                            <BookinglaneIcon/>
                    </WidgetIconContainer>
                    <WidgetTitleContainer>
                            <span className="widget-title">BOOK NOW!</span>
                    </WidgetTitleContainer>
            </WidgetContainer>
            </button>       
            <Modal show={this.store.ShowModal} onHide={this.store.closeModal}>
                <div style={{background: this.store.WidgetColors.BodyColor}} className="App">
                    <Wizzard store={this.store}/>
                </div>
            </Modal>
        </div>)
        }

        return (<div>
        <div className="widget-button" id="react-bookinglane-widget-button-id">
            <Draggable
                position={this.store.WidgetPosition}
                disabled={this.store.DragDisabled}
                onStart={this.store.lockAccordionButton}
                onStop={this.store.enableAccordionButton}>
            <Accordion defaultActiveKey="0" id="react-draggable-transform-block">                
                    <Accordion.Toggle eventKey="1" id="accordion-id" onClick={this.store.openWidget}>
                        <WidgetContainer>
                            <WidgetIconContainer>
                                <BookinglaneIcon/>
                            </WidgetIconContainer>
                            <WidgetTitleContainer>
                                <span className="widget-title">BOOK NOW!</span>
                            </WidgetTitleContainer>
                        </WidgetContainer>  
                    </Accordion.Toggle>           
                    <Accordion.Collapse eventKey="1">
                        <div style={{background: this.store.WidgetColors.BodyColor}} className="App">
                            <Wizzard
                                store={this.store}/>
                        </div>
                    </Accordion.Collapse>
            </Accordion>
            </Draggable>
        </div>
        </div>
        )
    }
}

export const Widget = WidgetComponent;

//#region styled-components


const WidgetTitleContainer = styled.div`
    height: 70px;
    font-family: IBM Plex Sans;
    font-style: normal;
    font-weight: 500;
    position: relative;
    top: 10px;
    left: 10px;
`;

const WidgetIconContainer = styled.div`
    height: 70px;
    position: relative;
    top: 4px;
    left: 2px;

`;

const WidgetContainer = styled.div`
    animation: pulse 2s infinite;
    border-radius: 50%;
    width: 150px;
    height: 150px;
    margin-bottom: 20px;
`;

//#endregion