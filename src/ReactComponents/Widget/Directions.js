import React from 'react';
import { toJS } from 'mobx';
const { compose, withProps, lifecycle } = require("recompose");
const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
} = require("react-google-maps");

export class Directions extends React.Component{
  constructor(props){
    super(props)
    this.store = this.props.store;
    this.waypoints = toJS(this.store.waypoints);
  }

  componentDidMount(){

  }
    render(){
        const store = this.props.store;

        const waypoints = toJS(store.waypoints);
        const startPlace = waypoints.find(x=>x.position == 0);
        const endPlace = waypoints.find(x=>x.position == 1);

        const onlyWaypoints = waypoints.filter(x=>x.position>1).sort((a, b) => (a.position > b.position) ? 1 : -1);
        const waypts = [];

        for(var i = 0; i < onlyWaypoints.length; i++){
            waypts.push({
                location: {
                    lat: onlyWaypoints[i].point.lat(),
                    lng: onlyWaypoints[i].point.lng()
                }
            });
        }

        const MapWithADirectionsRenderer = compose(
            withProps({
              googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAtq4TIxHsjoHKFgEtBD9V8weFHfiKDSaY&v=3.exp&libraries=geometry,drawing,places",
              loadingElement: <div style={{ height: `100%` }} />,
              containerElement: <div style={{ height: `300px` }} />,
              mapElement: <div style={{ height: `100%` }} />,
            }),
            withScriptjs,
            withGoogleMap,
            lifecycle({
              componentDidMount() {
                const DirectionsService = new google.maps.DirectionsService();
                
                if(waypoints.length>1){
                    DirectionsService.route({
                        origin: new google.maps.LatLng(startPlace.point.lat(), startPlace.point.lng()),
                        destination: new google.maps.LatLng(endPlace.point.lat(), endPlace.point.lng()),
                        waypoints: waypts,
                        travelMode: google.maps.TravelMode.DRIVING,
                      }, (result, status) => {
                        if (status === google.maps.DirectionsStatus.OK) {
                          this.setState({
                            directions: result,
                          });
                        } else {
                          console.error(`error fetching directions ${result}`);
                        }
                      });
                }
              }
            })
          )(props =>
            <GoogleMap
              defaultZoom={7}
            >
              {props.directions && <DirectionsRenderer directions={props.directions} />}
            </GoogleMap>
          );
          
          <MapWithADirectionsRenderer />
        return(<div>
            <MapWithADirectionsRenderer/>
        </div>)
    }
}

