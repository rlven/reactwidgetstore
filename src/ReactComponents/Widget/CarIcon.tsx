import React from 'react';
import { Sedan, Suv, Limo, MiniBus } from '../../Items/icons';

type Props = {
    carType: number
};

class CarIconComponent extends React.Component<Props>{
    
    render(){

        const carType = this.props.carType;

        let icon;
        if(carType===1){
            icon = <Sedan/>
        }
        else if(carType===2){
            icon = <Suv/>
        }
        else if(carType===3){
            icon = <MiniBus/>
        }
        else{
            icon = <Limo/>
        }

        return <div>
                {icon}
            </div>
    }
}

export const CarIcon = CarIconComponent;