import React from 'react';
import styled from 'styled-components';
import WidgetStore from './WidgetStore';
import { observer } from 'mobx-react';
import { EditPencilIcon, DoneIcon } from '../../Items/icons';
import {Directions} from './Directions';
import { toJS } from 'mobx';
import Carousel from 'nuka-carousel';

type Props = {
    store: WidgetStore
};

@observer
class OrderPreviewComponent extends React.Component<Props>{

    store: WidgetStore;
    constructor(props: any){
        super(props);
        this.store = this.props.store;
    }

    componentDidMount(){

    }

    render(){
        const pickedCar = toJS(this.store.pickedCar);

        return <div>
            <MainContainer>
                <OrderPreviewContainer>
                    <MapContainer>
                        <Directions
                        store={this.store}/>
                    </MapContainer>
                    <PreviewHeaderText>
                        <span>Preview</span>
                    </PreviewHeaderText>
                    <OrderDetailsContainer>
                        <OrderDetailsTextContainer>
                            <span>Date: {this.store.formData.date}</span>
                            <span>Time: {this.store.formData.time}</span>
                            <span>From: {this.store.formData.locationFromName}</span>
                            <span>To: {this.store.formData.locationToName}</span>
                            <span>Type of vehicle: {this.store.formData.carType}</span>
                            <span>Total distance: {this.store.totalDistance} miles</span>
                            <span>Amount: $ {this.store.OrderAmount}</span>
                        </OrderDetailsTextContainer>
                    </OrderDetailsContainer>
                    <CarInfoContainer>
                        <CarImageContainer>
                            <Carousel
                                renderCenterLeftControls={() => (
                                    <div></div>
                                )}
                                renderCenterRightControls={() => (
                                    <div></div>
                                )}>
                                {
                                pickedCar?.imageUrls.map((image: any, i: number) =>
                                    <img height="165px" src={image.path} key={i}/>
                                )}
                            </Carousel>
                        </CarImageContainer>
                        <CarContainer style={{background: this.store.WidgetColors.SecondaryColor}}>
                            <span className="car-info-type-preview">{pickedCar?.type != null ? pickedCar?.type : "Car type"}</span>
                            <span className="car-info-capacity-preview">Capacity: {this.store.pickedCar?.capacity}</span>
                            <span className="car-info-make-preview">{pickedCar?.make != null ? pickedCar?.make : "Make name"}</span>
                            <span className="car-info-model-preview">{pickedCar?.model != null ? pickedCar?.model : "Model name"}</span>
                            <CarPriceContainer className="car-price-container"><span className="fleet-choose-price">$ {this.store.OrderAmount}</span></CarPriceContainer>
                        </CarContainer>
                    </CarInfoContainer>
                    <OrderNotesContainer>
                        <span>Order Notes</span>
                    </OrderNotesContainer>
                    <OrderNotesAreaContainer id="order-notes-container" onClick={this.store.editNote}>
                        <div></div>
                        <textarea disabled id="order-area-notes-id" className="order-notes-area"></textarea>
                    </OrderNotesAreaContainer>
                    <OrderNotesBottomContainer id="order-notes-bottom-container-id">
                        <DoneIconContainer id="booking-widget-done-icon-container" onClick={this.store.confirmNote}>
                            <DoneIcon/> 
                        </DoneIconContainer>
                    </OrderNotesBottomContainer>
                    <ButtonContainer>
                        <button style={{background: this.store.WidgetColors?.SecondaryColor}} className="next-button" onClick={this.store.nextPage}>NEXT</button>
                    </ButtonContainer>
                </OrderPreviewContainer>
            </MainContainer>
            </div>
    }
}

export const OrderPreview = OrderPreviewComponent;

//#region styled-components

const OrderNotesBottomContainer = styled.div`
    height: 25px;
    background: #919191;
    width: 88%;
    margin: 0 auto;
    position: relative;
    top: -26px;
    @media (max-width: 500px) {
        top: -28px;
  }
`;

const OrderPreviewContainer = styled.div`
    margin: 0 auto;
    width: 90%;
`;

const MainContainer = styled.div`
    color: white;
`;

const MapContainer = styled.div`
    margin: 0 auto;
    width: 90%;
`;

const OrderDetailsContainer = styled.div`
    height: 130px;
    box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    background: #FFFFFF;
    margin: 0 auto;
    margin-top: 30px;
    width: 90%;
    overflow: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
`;

const OrderDetailsTextContainer = styled.div`
    color: black;
    display: inline-grid;
    margin-left: 20px;
    line-height: 1.1;
`;

const PreviewHeaderText = styled.div`
    height: 35px;
    border-bottom: 1px solid rgba(255, 255, 255, 0.12);
    margin-top: 20px;
    width: 95%;
`;

const CarImageContainer = styled.div`
    display: inline-block;
    width: 35%; 
    height: 136px;
    background: #5A5A5A;
`;

const CarContainer = styled.div`
    display: inline-block;
    width: 65%;
    height: 136px;
    padding-top: 30px;
`;

const CarInfoContainer = styled.div`
    margin: 0 auto;
    width: 90%;
    height: 136px;
    margin-top: 30px;
`;

const OrderNotesContainer = styled.div`
    height: 35px;
    border-bottom: 1px solid rgba(255, 255, 255, 0.12);
    margin-top: 30px;
    width: 95%;
    margin-top: 20px;
`;

const OrderNotesAreaContainer = styled.div`
    height: 100px;
    box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    background: #FFFFFF;
    width: 90%;
    margin: 0 auto;
    margin-top: 30px;
`;

const ButtonContainer = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 30px;
`;

const CarPriceContainer = styled.div`
    position: relative;
    display: block;
    font-size: 13px;
    left: 74%;
    top: -17%;
    width: 65px;
    height: 25px;
    background: white;
    border-radius: 4px;
    color: black;
    text-align: center;
    @media (max-width: 500px) {
        left: 64%;
  }
`;

const DoneIconContainer = styled.div`
    display: none;
    position: relative;
    left: 43%;
    text-align: center;
    bottom: 33%;
    @media (max-width: 500px) {
        left: 42%;
  }
`;

//#endregion