import React from 'react';
import styled from 'styled-components';
import WidgetStore from './WidgetStore';
import { observer } from 'mobx-react';
import { CloseWidgetIcon, BackArrowIcon } from '../../Items/icons';
import { toJS } from 'mobx';

type Props = {
    store: WidgetStore
};

@observer
class CancelationPolicyComponent extends React.Component<Props>{
    
    render(){
        const store = this.props.store;
        const companyInfo = toJS(store.CompanyInfo);
        const cancelationFees = toJS(store.pickedCar.cancelationFees);

        const mappedFees = cancelationFees.map((fee, key) => 
        <PolcyContainer key={key}>
            <HoursContainer>
                {fee.chargeStartFrom.toString().padStart(2, '0')} {fee.chargeStartTo !== 0 ? " - "+fee.chargeStartTo.toString().padStart(2, '0') : "+"}
            </HoursContainer>
            <PercentContainer>
                {fee.chargePercentage} % refundable
            </PercentContainer>
        </PolcyContainer>
        );

        return <Container>
                    <CompanyInfoContainer style={{background: store.WidgetColors.TitleColor}}>
                        {store.WizzardCurrentPage !== 1 &&
                        <BackArrow onClick={store.closeCancelationPolicy}>
                            <BackArrowIcon/>
                        </BackArrow>
                        }
                        <CompanyLogo>
                            <img className="company-logo-image" src={companyInfo?.companyLogoPath}/>
                        </CompanyLogo>
                        <CompanyName>
                            {companyInfo?.companyName != undefined ? companyInfo.companyName : "Company name"}
                        </CompanyName>
                        <CloseWidget onClick={store.closeWidget}>
                            <CloseWidgetIcon/>
                        </CloseWidget>
                    </CompanyInfoContainer>
                    <CancelationContainer>
                         <CancelationTitle>Cancellation policy</CancelationTitle>
                         <PolcyContainer>
                            <HoursContainer>
                                Hours prior
                            </HoursContainer>
                            <PercentContainer>
                                Refund percentage
                            </PercentContainer>
                        </PolcyContainer>
                        {mappedFees}
                    </CancelationContainer>
            </Container>
    }
}

export const CancelationPolicy = CancelationPolicyComponent;

//#region styled-components
const Container = styled.div`
    color: white;
`; 

const CancelationTitle = styled.h4`
    margin-top: 20px;
    color: white;
`; 

const CancelationContainer = styled.div`
    color: white;
    margin 0 auto;
    width: 80%;
`; 

const PolcyContainer = styled.div`
    margin-top: 10px;
    width: 100%;
    height: 40px;
    display: inline-block;
`; 

const HoursContainer = styled.div`
    border-bottom: 1px solid rgba(255, 255, 255, 0.12);
    width: 35%;
    height: 100%;
    display: inline-block;
`; 

const PercentContainer = styled.div`
    border-radius: 5px;
    width: 65%;
    height: 100%;
    display: inline-block;
    border-bottom: 1px solid rgba(255, 255, 255, 0.12);
`; 

const CloseWidget = styled.div`
    display: inline-block;
    float: right;
    margin-right: 10px;
    margin-top: 10%;
    @media (max-width: 500px) {
        margin-top: 12%;
  }
`;

const BackArrow = styled.div`
    display: inline-block;
    float: left;
    margin-left: 2%;
    height: 40px;
    width: 40px;
    margin-top: 10%;
    @media (max-width: 500px) {
        margin-top: 13%;
  }
`;

const CompanyInfoContainer = styled.div`
    height: 130px;
    border-bottom: 1px solid #C4C4C4;
    position: sticky;
    top: 0;
    z-index: 1;
    color: white;
`;

const CompanyLogo = styled.div`
    display: inline-block;
    border-radius: 50%;
    background: white;
    height: 75px;
    width: 75px;
    position: absolute;
    top:23%;
    left:23%;
    @media (max-width: 500px) {
        left: 9%;
  }
`;

const CompanyName = styled.div`
    display: inline-block;
    font-size: 24px;
    margin: 0;
    position: absolute;
    top: 34%;
    left: 42%;
    @media (max-width: 500px) {
        left: 34%;
  }
`;

//#endregion