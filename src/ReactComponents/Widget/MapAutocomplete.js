import React, { Component } from "react";
import { Map, Marker, GoogleApiWrapper, Polyline } from "google-maps-react";
import PlacesAutocomplete from "react-places-autocomplete";
import { LocationIcon, DestinationIcon } from '../../Items/icons';
import styled from 'styled-components';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { PlusIcon } from '../../Items/icons';
import { add } from "lodash-es";

@observer
export class LocationSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { address: "" };
    this.store = this.props.store;
    this.propName = this.props.inputFieldName;
    this.position = this.props.position;
    this.placesService = new window.google.maps.places.PlacesService(
      document.createElement("div")
    );

    this.handleChange = this.handleChange.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleChange = (address) => {
    this.setState({ address });
    switch(this.props.inputName) {
      case "startPoint":
        this.store.LocationFromName = address;
        break;
      case "endPoint":
        this.store.LocationToName = address;
        break;
      case "endpoint2":
        this.store.LocationName2 = address;
        break;
      case "endpoint3":
        this.store.LocationName3 = address;
        break;
      case "endpoint4":
        this.store.LocationName4 = address;
        break;
      case "endpoint5":
        this.store.LocationName5 = address;
        break;
      default:           
        break;
    }

  };

  handleSelect = (address, placeId) => {
    this.setState({ address });
    
    switch(this.props.inputName) {
      case "startPoint":
        this.store.LocationFromName = address;
        this.store.formData.locationFromName = address;
        var placeidInfo = {id: placeId, position: 0}
        break;
      case "endPoint":
        this.store.LocationToName = address;
        this.store.formData.locationToName = address;
        var placeidInfo = {id: placeId, position: 1}
        break;
      case "endpoint2":
        this.store.LocationName2 = address;
        var placeidInfo = {id: placeId, position: 2}
        break;
      case "endpoint3":
        this.store.LocationName3 = address;
        var placeidInfo = {id: placeId, position: 3}
        break;
      case "endpoint4":
        this.store.LocationName4 = address;
        var placeidInfo = {id: placeId, position: 4}
        break;
      case "endpoint5":
        this.store.LocationName5 = address;
        var placeidInfo = {id: placeId, position: 5}
        break;
      default:           
        break;
    }
    this.store.placeIds.push(placeidInfo);

    const request = {
      placeId: placeId,
      fields: ["name", "geometry", "type", "address_component"]
    };
    this.placesService.getDetails(request, (place, status) => {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        this.props.onPlaceChanged(place, this.position);
        
        switch(this.props.inputName) {
          case "startPoint":
            if(!this.store.locationToIsAirport){
              this.store.AirportTransferFormShow = false;
              this.store.formData.airportTransfer = false;
            }
            place.types.forEach(element => {
              if(element==="airport"){
                this.store.AirportTransferFormShow = true;
                this.store.formData.airportTransfer = true;
                this.store.formData.airportTransferInfo.Direction = true;
                this.store.locationFromIsAirport = true;
              }
            });
            if(this.store.locationToType=="airport"){
              this.store.AirportTransferFormShow = true;
              this.store.formData.airportTransfer = true;
            }
        //    this.store.formData.locationFromName = place.name;
            this.store.formData.locationFrom.longitude = place.geometry.location.lng()
            this.store.formData.locationFrom.latitude = place.geometry.location.lat()
            break;
          case "endPoint":
            if(!this.store.locationFromIsAirport){
              this.store.AirportTransferFormShow = false;
              this.store.formData.airportTransfer = false;
            }
            place.types.forEach(element => {
              if(element==="airport"){
                this.store.AirportTransferFormShow = true;
                this.store.formData.airportTransfer = true;
                this.store.formData.airportTransferInfo.Direction = false;
                this.store.locationToIsAirport = true;
              }
            });
            if(this.store.locationFromType=="airport"){
              this.store.AirportTransferFormShow = true;
              this.store.formData.airportTransfer = true;
            }
       //     this.store.formData.locationToName = place.name;
            this.store.formData.locationTo.longitude = place.geometry.location.lng()
            this.store.formData.locationTo.latitude = place.geometry.location.lat()
            break;
          default:           
            break;
        }
      }
    });
  };

  render() {
    let locationPlace = '';
    switch(this.props.inputName) {
      case "startPoint":
        locationPlace = this.store.LocationFromName;
        break;
      case "endPoint":
        locationPlace = this.store.LocationToName;
        break;
      case "endpoint2":
        locationPlace = this.store.LocationName2
        break;
      case "endpoint3":
        locationPlace = this.store.LocationName3;
        break;
      case "endpoint4":
        locationPlace = this.store.LocationName4;
        break;
      case "endpoint5":
        locationPlace = this.store.LocationName5;
        break;
      default:
        locationPlace = this.state.address;     
        break;
    }
    return (
      <PlacesAutocomplete
        value={locationPlace == undefined ? this.state.address: locationPlace}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) =>   (
          <div className="autocomplete-container">
            <input id={this.propName}
              {...getInputProps({
                className: "location-search-input form-area-from-input"
              })}
            />
            <div className="autocomplete-dropdown-container autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map((suggestion) => {
                const className = suggestion.active
                  ? "suggestion-item--active"
                  : "suggestion-item";
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: "#ffffff", cursor: "pointer" }
                  : { backgroundColor: "#ffffff", cursor: "pointer" };
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style
                    })}
                  >
                    <span className="autocomplete-places-text">{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

@observer
class MapContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { places: [] };
    this.store = this.props.store;
    this.showPlace = this.showPlace.bind(this);
    this.mapRef = React.createRef();
  }

  showPlace(place, position) {

    this.setState((prevState) => ({
      places: [...prevState.places, place]
    }));
    let placeType = 1;
    place.types.forEach(element => {
      if(element==="airport"){
        placeType=2;
      }
    });

    const placeId = this.store.placeIds.find(x=>x.position === position).id;
    this.store.addLocation(place.geometry.location, position, place.name, placeId, placeType);

    this.mapRef.current.map.setCenter(place.geometry.location);
    this.store.markerLat = place.geometry.location.lat();
    this.store.markerLng = place.geometry.location.lng();
  }

  render() {

    const additionalDestinations = [];

    for (var i = 1; i < this.store.LocationsCount; i++) {
        let inputName = "endpoint"+(i+1);
        let name = this.props.name+i;
        additionalDestinations.push(
        <div key={i} className="location-container-input">
            <InputLocationIcon>
                <DestinationIcon/>
            </InputLocationIcon>
            <InputLocationField>
                <LocationSearchInput position={i+1} inputFieldName={name} store={this.store} 
                                    inputName={inputName} onPlaceChanged={this.showPlace} />
          </InputLocationField>
          <div className="form-area-add-destination-from">
             To:
          </div>
              <div onClick={()=>this.store.removeDestination(i)} className="form-area-add-destination">
                <div style={{width: "10px", transform: "rotate(45deg)"}}>
                  <PlusIcon/>
                </div>
              </div>
        </div>
      )};

    return (
      <div className="map-container">   
        <Map    
          ref={this.mapRef}
          google={this.props.google}
          className={"map"}
          zoom={16}
          initialCenter={this.props.center}
          style={style}
          containerStyle={containerStyle}
        >
        <Marker
          title={'The marker`s title will appear as a tooltip.'}
          name={'SOMA'}
          position={{lat: this.store.markerLat, lng: this.store.markerLng}} />
        </Map>
        <InputLocationContainer>
          <div className="location-container-input">
              <InputLocationIcon>
                  <LocationIcon/>
              </InputLocationIcon>
              <InputLocationField>
                  <LocationSearchInput position={0} inputFieldName="booking-location-from-name" 
                                        store={this.store} inputName="startPoint" onPlaceChanged={this.showPlace} />
            </InputLocationField>
            <div className="form-area-add-destination-from">
             From:
            </div>
          </div>
          <div className="location-container-input">
              <InputLocationIcon>
                  <DestinationIcon/>
              </InputLocationIcon>
              <InputLocationField>
                  <LocationSearchInput position={1} inputFieldName="booking-location-to-name" 
                                        store={this.store} inputName="endPoint" onPlaceChanged={this.showPlace} />
              </InputLocationField>
            <div className="form-area-add-destination-from">
             To:
            </div>
              <div onClick={()=>this.store.addDestionationLocation()} className="form-area-add-destination">
                <div style={{width: "10px"}}>
                  <PlusIcon/>
                </div>
              </div>
          </div>
          {additionalDestinations}
        </InputLocationContainer>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyAtq4TIxHsjoHKFgEtBD9V8weFHfiKDSaY",
  libraries: ["places", "drawing", "geometry"]
})(MapContainer);

const style = {
    width: '100%',
    height: '300px'
  }

const containerStyle = {
    position: 'relative',  
    width: '100%',
    height: '300px'
  }

const InputLocationContainer = styled.div`
    margin-top: 10px;   
    padding: 10px;
    margin-left: 8px;
    width: 100%;
`;

 const InputLocationField = styled.div`
    display:inline-block;
    font-size: 10px;
    margin-left: 5px;
    width: 91%;
    margin-left: 4.5%;
`;

const InputLocationIcon = styled.div`
    display:inline-block;
    font-size: 10px;
    margin-top: 4px;
    width: 4%;
    color: white;
    position: absolute;
`;

