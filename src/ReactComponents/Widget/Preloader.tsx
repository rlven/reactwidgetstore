import React from 'react';
import styled from 'styled-components';
import Lottie from 'react-lottie';
import * as animationData from './Anim.json';

class PreloaderComponent extends React.Component{
    
    render(){

        const defaultOptions = {
            loop: true,
            autoplay: true, 
            animationData: animationData.default,
            rendererSettings: {
              preserveAspectRatio: 'xMidYMid slice'
            }
          };

        return <Container>
            <Lottie options={defaultOptions}
              height={236}
              width={200}/>
        </Container>
    }
}

export const PreloaderComp = PreloaderComponent;

//#region styled-components

const Container = styled.div`
    margin: 0 auto;
    width: 100%;
    margin-top: 100px;
    /* @media (max-width: 500px) {
        width: 94%;
    } */
`;

//#endregion