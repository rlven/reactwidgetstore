import { observable } from 'mobx';
import RequestHelper from '../../Helpers/RequestHelper';
import {CompanyInfoResponse} from '../../Helpers/DataContracts';
import {ReservationForm, RiderDetails, OrderAddress, PaymentForm, Car, CalculateRequest, Airlines} from '../../Helpers/DataContracts';
import {validate, errorIds} from '../../Helpers/FormValidator';
import {validatePaymentForm, paymentErrorIds} from '../../Helpers/PaymentFormValidator';
import {WidgetPosition, WidgetColors, ClientInfo} from './DataContracts';
import swal from 'sweetalert';
import {
    geocodeByAddress,
    getLatLng
  } from 'react-places-autocomplete';

export default class WidgetStore{
    
    AirlinesList: Airlines[] = [];
    WidgetColors: WidgetColors = {
        TitleColor: "#131126",
        BodyColor: "#131126",
        SecondaryColor: "radial-gradient(86% 4845.12% at 6.25% 14%, #568AFD 0%, #670DF2 49.48%, #851EDF 100%)"
    };
    accessKey: string = "";
    drag: boolean = true;
    areaDisabled = true;
    isHourly: boolean = false;
    waypoints: {position: number, point: google.maps.LatLng, locationName: string, placeType: number, placeId: string} [] | [] = [];
    pickedCar: Car | null = null;
    cartypeCapacity: number = 0;
    locationFromType: string = "";
    locationToType: string = "";
    riderDetails: RiderDetails = {
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber: ""
    };
    locationFromIsAirport: boolean = false;
    locationToIsAirport: boolean = false;
    formData: any = {
        locationFromName: "",
        locationToName: "",
        locationFrom:{
            longitude: 0,
            latitude: 0
        },
        locationTo: {
            longitude: 0,
            latitude: 0
        },
        date: '',
        time: '',
        peopleCount: 0,
        hoursCount: 0,
        airportTransfer: false,
        isHourly: false,
        carTypeId: 0,
        orderNotes: "",
        totalDistance: 0,
        totalDuration: 0,
        amount: 0,
        carType: "",
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber: "",
        address: "",
        city: "",
        state: "",
        zip: "",
        cardNumber: "",
        pickedCarId: 0,
        pickedCarTypeId: 0,
        airportTransferInfo: {
            Airlines: '',
            FlightNumber: '',
            GateMeeting: false,
            Direction: false
        },
        cardInfo: {
            cardNumber: '',
            month: 0,
            year: 0,
            cvc: '',
            amount: 0
        }
    };
    placeIds: any[] = [];

    AirlinesSelected:Airlines | null = null;
    ArrowDissapear:boolean = false;
    @observable IsTextAreaOpened: boolean = false;
    @observable TotalDistance: number = 0;
    @observable TotalDuration: number = 0;
    @observable TermOfUseChecked:boolean = false;
    @observable CancelatonChecked:boolean = false;
    @observable PaymentFirstName: string = '';
    @observable PaymentLastName: string = '';
    @observable PaymentEmail: string = '';
    @observable PaymentPhone: string = '';
    @observable PaymentAddress: string = '';
    @observable PaymentCity: string = '';
    @observable PaymentState: string = '';
    @observable PaymentZip: string = '';
    @observable PaymentCard: string = '';
    @observable PaymentMonthYear: string = '';
    @observable PaymentCVC: string = '';
    @observable CancelationPolicyOpened: boolean = false;
    @observable TermOfUseOpened: boolean = false;
    @observable WidgetPosition: WidgetPosition | null = null;
    @observable IsPaymentFinish: boolean = false;
    @observable LocationName2: string = '';
    @observable LocationName3: string = '';
    @observable LocationName4: string = '';
    @observable LocationName5: string = '';
    @observable DragDisabled: boolean = false;
    @observable LocationFromName: string = '';
    @observable LocationToName: string = '';
    @observable Airlines: string = '';
    @observable FlightNumber: string = '';
    @observable fullDate: string = '2021-01-01T00:00';
    @observable time: string = '12:00';
    @observable Cars: Car[] = [];
    @observable IsHourly: boolean = false;
    @observable OrderAmount: number = 0;
    @observable CompanyInfo: CompanyInfoResponse | null = null;
    @observable PeopleCount: number = 0
    @observable HoursCount: number = 0;
    @observable AirportTransferFormShow: boolean = false;
    @observable LocationsCount: number = 1;
    @observable CurrentLocation: string = '';
    @observable WizzardCurrentPage: number = 1;
    @observable RiderDetailsChecked: boolean = false;
    @observable pickedCarTypeId: number | null = null;
    @observable pickedCarId: number = 0;
    @observable markerLat: number = 0;
    @observable totalDistance: number = 0;
    @observable markerLng: number = 0;
    @observable ShowModal: boolean = false;

    openModal = (): void => {
        this.ShowModal = true;
    }

    closeModal = (): void => {
        this.ShowModal = false;
    }

    removePeople = (): void => {
        if(this.PeopleCount!==0){
            this.PeopleCount--;
            this.formData.peopleCount = this.PeopleCount;
        }
    }

    editNote = () => {
        const textArea = document.getElementById('order-area-notes-id');
        textArea?.removeAttribute('disabled');
        var element = document.getElementById("booking-widget-done-icon-container");
        element.style.display = "block";
        textArea.style.background = "white";
        document.getElementById('order-notes-bottom-container-id').style.background = "white";

    }

    confirmNote = () => {
        const textArea = document.getElementById('order-area-notes-id');
        textArea?.setAttribute('disabled', 'true');
        var element = document.getElementById("booking-widget-done-icon-container");
        element.style.display = "none";
        this.formData.orderNotes = textArea.value;
        textArea.style.background = "#919191";
        document.getElementById('order-notes-bottom-container-id').style.background = "#919191";


    }

    addLocation = (location: any, position: number, locationName: string, placeId: string, placeType:number): void => {
        var newLocation = {position: position, point:new google.maps.LatLng(location.lat(), location.lng()), locationName: locationName, placeId: placeId,placeType:placeType };
        this.waypoints = this.waypoints.filter(f=>f.position!==position);
        this.waypoints.push(newLocation);
        this.Cars = [];
    }

    addPeople = (): void => {
        if(this.PeopleCount < 12){
            this.PeopleCount++;
            this.formData.peopleCount = this.PeopleCount;
        }
    }

    deactivateHourly = () => {
        this.IsHourly = false;
        this.formData.isHourly = false;
        this.HoursCount = 0;
    }

    activateHourly = () => {
        this.IsHourly = true;
        this.formData.isHourly = true;
    }

    addHour = ():void => {
        if(this.HoursCount < 12){
            this.HoursCount++;
            this.formData.hoursCount = this.HoursCount;
        }
    }

    minusHour = ():void => {
        if(this.HoursCount != 0){
            this.HoursCount--;
            this.formData.hoursCount = this.HoursCount;
        }
    }

    showAirportTransferForm = (): void => {
        this.AirportTransferFormShow = true;
        this.formData.airportTransfer = true;
    }

    hideAirportTransferForm = (): void => {
        this.AirportTransferFormShow = false;
        this.formData.airportTransfer = false;
    }

    addDestionationLocation = (): void => {
        if(this.LocationsCount<5){
            this.LocationsCount++;
        }

    }

    removeDestination = (position: number) => {
        switch(position){
            case 2:
                this.LocationName2 = '';
                break;
            case 3:
                this.LocationName3 = '';
                break;
            case 4:
                this.LocationName4 = '';
                break;
            case 5:
                this.LocationName5 = '';
                break;
        }
        this.waypoints = this.waypoints.filter(f=>f.position!==position);
        this.placeIds = this.placeIds.filter(f=>f.position!==position);
        this.LocationsCount--;
    }

    handleGoogleAutocompleteChange = (value: any): void =>{
        this.CurrentLocation = value
    }

    handleGoogleAutocompleteSelect = (address: any): void =>{
        geocodeByAddress(address)
          .then((results:any) => getLatLng(results[0]))
          .then((latLng: any) => console.log('Success', latLng))
          .catch((error:any) => console.error('Error', error));
      };

    openWidget = (): void => {
        var accrodionButton = document.getElementById('accordion-id');
        accrodionButton?.setAttribute('hidden', 'true');
        var element = document.getElementById("accordion-id");
        element.style.pointerEvents = "auto";
        this.DragDisabled = true;

        const appWidth = 500;
        const appHeight = 700;

        var windowWidth = window.innerWidth;
        var windowHeight = window.innerHeight;

        var transformBlock = document.getElementById('react-draggable-transform-block');
        var transformInfo = transformBlock?.style.transform;
        var widthTransition = Number(transformInfo.substring(
            transformInfo.lastIndexOf("(") + 1, 
            transformInfo.lastIndexOf("px,")
        ));
        var heightTransition = Number(transformInfo.substring(
            transformInfo.lastIndexOf(", ") + 1, 
            transformInfo.lastIndexOf("px)")
        ));

        var positiveHeightTransition = Math.abs(heightTransition);
        var maxWidthPostiion = windowWidth - appWidth;
        var maxHeightPosition = windowHeight - appHeight;
        
        if(heightTransition > 0){
            this.WidgetPosition = {x: widthTransition, y: 0};
        }

        if(widthTransition < 0){
            this.WidgetPosition = {x: 0, y: heightTransition};
        }

        if(heightTransition > 0 && widthTransition < 0){
            this.WidgetPosition = {x: 0, y: 0};
        }

        if(positiveHeightTransition > maxHeightPosition){
            if(widthTransition > maxWidthPostiion){
                this.WidgetPosition = {x: maxWidthPostiion-50, y: -Math.abs(maxHeightPosition)};
            }
            else{
                this.WidgetPosition = {x: widthTransition, y: -Math.abs(maxHeightPosition)};
            }
            if(widthTransition < 0){
                this.WidgetPosition = {x: 0, y: -Math.abs(maxHeightPosition)};
            }
        }
        else{
            if(widthTransition > maxWidthPostiion){
                this.WidgetPosition = {x: maxWidthPostiion, y: 0};
            }
        }

        document.getElementById('booking-location-from-name').removeAttribute("type");
        document.getElementById('booking-location-to-name').removeAttribute("type");
    }

    closeWidget = (): void => {
        document.getElementById('accordion-id')?.click();
        document.getElementById('accordion-id')?.removeAttribute('hidden');
        var element = document.getElementById("accordion-id");
        element.style.pointerEvents = "auto";
        this.DragDisabled = false;
        this.WidgetPosition = null;
    }

    nextPage = (): void => {
        this.WizzardCurrentPage++;
    }

    prevPage = (): void => {
        this.WizzardCurrentPage--;
    }

    expandRiderDetails = (): void => {
        this.RiderDetailsChecked = true;
    }

    hideRiderDetails = (): void => {
        this.RiderDetailsChecked = false;
    }

    cancelAll = (): void => {
        this.WizzardCurrentPage = 1;
        this.IsPaymentFinish = false;
        this.closeWidget();
    }

    requireGateMeeting = (): void => {
        this.formData.airportTransferInfo.GateMeeting = true;
    }

    noGateMeeting = (): void => {
        this.formData.airportTransferInfo.GateMeeting = false;
    }

    aiportDirectionFrom = (): void => {
        this.formData.airportTransferInfo.Direction = true;
    }

    aiportDirectionTo = (): void => {
        this.formData.airportTransferInfo.Direction = false;
    }

    extractKeyFromScrptSrc = (): void => {
        let scriptSrc = "";
        const scripts = document.getElementsByTagName("script");

        for( var i =0;i < scripts.length; i++){
            if(scripts[i].outerHTML.includes("?bookinglaneWidgetAccessKey=")){
                scriptSrc = scripts[i].outerHTML;
            }
        }
        this.accessKey = scriptSrc.substring(
            scriptSrc.lastIndexOf("=") + 1, 
            scriptSrc.lastIndexOf("\"")
        )
    };

    fetchCompanyInfo = () => {
        RequestHelper.authByAccessKey(this.accessKey).then(result => {
            window.localStorage.setItem("AuthToken", JSON.stringify(result));
            RequestHelper.getCompanyInfo(result.jwtToken).then(result => {
                this.CompanyInfo = result;
            });
        });
    }

    fetchCompanyCarsWithPrice = () => {
        if(this.AirportTransferFormShow){
            this.calculateAirportTransferPrice();
        }
        else if(this.IsHourly){
            this.calculateHourlyPrice();
        }
        else{
            this.calculateMileagePrice();
        }
    }

    calculateAirportTransferPrice = () => {
        var token = window.localStorage.getItem("AuthToken");
        token = JSON.parse(token);
        const OrderAddress = this.fillAddresses();
        const calculateRequest: CalculateRequest = {
            page: 1,
            typeId: this.formData.pickedCarTypeId,
            OrderAddressDetails: OrderAddress,
            isGateMeeting: this.formData.airportTransferInfo.GateMeeting
        }

        RequestHelper.calculateAirportInclPrice(token.jwtToken, calculateRequest)
            .then(result => {
                this.Cars = result;
            });
    }

    calculateMileagePrice = () => {
        var token = window.localStorage.getItem("AuthToken");
        token = JSON.parse(token);
        const OrderAddress = this.fillAddresses();
        const calculateRequest: CalculateRequest = {
            page: 1,
            typeId: this.formData.pickedCarTypeId,
            OrderAddressDetails: OrderAddress,
            isGateMeeting: false
        }

        RequestHelper.calculateMileagePrice(token.jwtToken, calculateRequest)
            .then(result => {
                this.Cars = result;
            });
    }

    calculateHourlyPrice = () => {
        var token = window.localStorage.getItem("AuthToken");
        token = JSON.parse(token);
        RequestHelper.calculateHourlyPrice(token.jwtToken, this.formData.hoursCount, this.formData.pickedCarTypeId)
            .then(result => {
                this.Cars = result;
            });
    }

    pickFleet = (carId: number): void => {
        this.pickedCar = this.Cars.find(x=>x.id==carId);
        this.pickedCarId = carId;
        this.formData.pickedCarId = carId;
        this.OrderAmount = (this.pickedCar.price).toFixed(2);
        this.formData.amount = this.pickedCar.price;
    }

    pickCarType = (carTypeId: number, capacity: number, type: string): void => {
        this.pickedCarTypeId = carTypeId;
        this.cartypeCapacity = capacity;
        this.formData.carType = type;
        this.formData.pickedCarTypeId = carTypeId;
    }

    submitPaymentForm = () => {

        const errors = validatePaymentForm(this.formData, !this.RiderDetailsChecked, this.riderDetails);
        for(var i = 0; i< paymentErrorIds.length; i++){
            var element = document.getElementById(paymentErrorIds[i]);
            element?.setAttribute("style", "border: none;");;
        }
        if(errors.length<1){
            this.makePayment();
        }
        else{
            for(var i = 0; i< errors.length; i++){
                var element = document.getElementById(errors[i]);
                element?.setAttribute("style", "border: 1px solid red;");;
            }
            swal('Please fill required fields');
        }
    }

    fillAddresses(){
        let OrderAddress: OrderAddress[] = [];

        for(var i = 0; i < this.waypoints.length; i++){
            let orderAddress: OrderAddress = {
                latitude: this.waypoints[i].point.lat(),
                longitude: this.waypoints[i].point.lng(),
                pointIndexNum: i + 1,
                rideCheckPoint: this.waypoints[i].locationName,
                placeId: this.waypoints[i].placeId,
                placeType: this.waypoints[i].placeType
            }
            OrderAddress.push(orderAddress);
        }
        return OrderAddress;
    }

    makePayment = () => {
        this.WizzardCurrentPage++;

        let riderDetails: RiderDetails = {
            firstName: this.riderDetails.firstName,
            lastName: this.riderDetails.lastName,
            email: this.riderDetails.email,
            phoneNumber: this.riderDetails.phoneNumber
        }

        let OrderAddress = this.fillAddresses();

        var clientInfo: ClientInfo = {
            firstName: this.formData.firstName,
            lastName: this.formData.lastName,
            phoneNumber: this.formData.phoneNumber,
            email: this.formData.email
        }

        let form: ReservationForm = {
            orderStartDateTime: this.fullDate,
            totalOrderMileage: +this.formData.totalDistance,
            totalMinutes: +this.formData.totalDuration,
            passengersQuantity: this.PeopleCount,
            carInfo: {id: this.pickedCarId},
            orderSum: +this.formData.amount,
            orderNotes: this.formData.orderNotes,
            riderDetails: riderDetails,
            OrderAddressDetails: OrderAddress,
            isAirportPickupIncluded: this.formData.airportTransferInfo.GateMeeting,
            hours: this.formData.hoursCount,
            orderType: 3,
            clientInfo: clientInfo
        }

        let card: PaymentForm = {
            CardNumber: this.formData.cardNumber,
            Month: Number(this.formData.month),
            Year: Number(this.formData.year),
            Cvc: this.formData.cvc,
            Amount: Number(this.formData.amount*100)
        }
        form.PaymentInfo = card;

        if(this.formData.airportTransfer){
            form.bookingType = 3;
            form.airlines = {id: this.AirlinesSelected?.id}
            form.flightNumber = this.AirlinesSelected?.code+this.formData.airportTransferInfo.FlightNumber;
        }
        else if(this.IsHourly){
            form.bookingType = 2;
        }
        else{
            form.bookingType = 1;
        }

        RequestHelper.createReservation(form).then(result => {
                
            swal("Order placed");
           // this.resetPaymentForm();
            this.IsPaymentFinish=true;
            this.cancelAll();
        }).catch(error => {
                //this.resetPaymentForm();
                this.WizzardCurrentPage--;
                console.log(error.response.data[0]);
                swal(error.response.data[0]);
        });

    }

    handleDate = (event: any): void => {
        const date = event.target.value;

        const pickedDate = new Date(date);
        const formatedDate = ((pickedDate.getMonth() > 8) ? (pickedDate.getMonth() + 1) 
        : ('0' + (pickedDate.getMonth() + 1))) 
        + '/' + ((pickedDate.getDate() > 9) 
        ? pickedDate.getDate() 
        : ('0' + pickedDate.getDate())) 
        + '/' + pickedDate.getFullYear();
        this.fullDate = date;
        this.formData.date = formatedDate;
        let timeConvention;
        let hour = pickedDate.getHours();
        let minutes = pickedDate.getMinutes();
        if(hour>12){
            hour = hour-12;
            timeConvention = 'pm';
        }
        else{
            timeConvention = 'am';
        }
        const formatedMinutes = ('0' + (minutes)).slice(-2);
        const formatedTime = hour+":"+formatedMinutes+" "+timeConvention;
        this.formData.time = formatedTime;  
    }

    handleTime = (event: any):void => {
        const time = event.target.value;
    }
    
    checkCarType(carTypeId: number){
        if(carTypeId == this.pickedCarTypeId){
            return true;
        }
        return false;
    }
    setDistance = (distance: number) =>{
        this.formData.totalDistance = distance;
        this.totalDistance = distance;
        this.TotalDistance = distance;
    }
    setDuration = (duration: number) => {
        this.formData.totalDuration = duration;
        this.TotalDuration = duration;
    }

    handleFirstName = (event: any) => {
        this.formData.firstName = event?.target.value;
        this.PaymentFirstName = event?.target.value;
    }
    handleLastName = (event: any) => {
        this.formData.lastName = event?.target.value;
        this.PaymentLastName = event?.target.value;
    }
    handleEmail = (event: any) => {
        this.formData.email = event?.target.value;
        this.PaymentEmail = event?.target.value;
    }
    handlePhone = (event: any) => {
        this.formData.phoneNumber = event?.target.value;
        this.PaymentPhone = event?.target.value;
    }
    
    handleRiderFirstName = (event: any) => {
        this.riderDetails.firstName = event?.target.value;
    }
    handleRiderLastName = (event: any) => {
        this.riderDetails.lastName = event?.target.value;
    }
    handleRiderEmail = (event: any) => {
        this.riderDetails.email = event?.target.value;
    }
    handleRiderPhone = (event: any) => {
        this.riderDetails.phoneNumber = event?.target.value;
    }
    handleCardNumber = (event: any) => {
        this.formData.cardNumber = event?.target.value.slice(0, -1);
        this.PaymentCard = event?.target.value.slice(0, -1);
    }
    handleCardCvcValid = (event: any) => {
        let inputData = event?.target.value;
        this.PaymentMonthYear = event?.target.value;
        var year = inputData.substring(inputData.indexOf('/') + 1).slice(0, -1);
        var month = inputData.substr(0, inputData.indexOf('/')); 
        this.formData.month = month;
        this.formData.year = year;
    }

    handleCvc = (event: any) => {
        this.formData.cvc = event?.target.value.slice(0, -1);;
        this.PaymentCVC = event?.target.value;
    }

    submitForm = () => {
        const errors = validate(this.formData, this.IsHourly, this.AirportTransferFormShow);
        for(var i = 0; i< errorIds.length; i++){
            var element = document.getElementById(errorIds[i]);
            element?.setAttribute("style", "border: none;");;
        }
        if(errors.length<1){
            this.nextPage();
        }
        else{
            for(var i = 0; i< errors.length; i++){
                var element = document.getElementById(errors[i]);
                element?.setAttribute("style", "border: 1px solid red;");;
            }
            swal('Please fill required fields');
        }
    }

    calculateCarPrice = () => {
        if(this.isHourly){
            this.calculateHourlyPrice();
        }
        else{
            this.calculateMileagePrice();
        }
    }
    
    handleAirlinesInput = (event: any) => {
        this.formData.airportTransferInfo.Airlines = event?.target.value;
        this.Airlines = event?.target.value;
        this.AirlinesSelected = this.AirlinesList.find(x=>x.name===this.Airlines);
    }

    handleFlightNumberInput = (event: any) => {
        this.formData.airportTransferInfo.FlightNumber = event?.target.value;
        this.FlightNumber = event?.target.value;
    }

    lockAccordionButton = () => {
        var element = document.getElementById("accordion-id");
        setTimeout(()=>element.style.pointerEvents = "none", 200);
        
    }
    enableAccordionButton = () => {
        var element = document.getElementById("accordion-id");
        element.style.pointerEvents = "auto";
        setTimeout(()=>element.style.pointerEvents = "auto", 400);
    }

    handlePaymentAddress = (event: any) => {
        this.formData.address = event.target.value;
        this.PaymentAddress = event.target.value;
    }
    handleCity = (event: any) => {
        this.formData.city = event.target.value;
        this.PaymentCity = event.target.value;
    }
    handleState = (event: any) => {
        this.formData.state = event.target.value;
        this.PaymentState = event.target.value;
    }
    handleZip = (event: any) => {
        this.formData.zip = event.target.value;
        this.PaymentZip = event.target.value;
    }
    resetPaymentForm = () => {
        this.formData.firstName = '';
        this.formData.lastName = '';
        this.formData.email = '';
        this.formData.phoneNumber = '';
        this.formData.address = '';
        this.formData.city = '';
        this.formData.state = '';
        this.formData.zip = '';
        this.formData.cardNumber = '';
    }

    hideArrow = () => {
        if(!this.ArrowDissapear){
            document.getElementById('bookinglane-form-area-down-arrow-id').style.display = 'none';
            this.ArrowDissapear = true;
        }
    }

    openTermOfUse = () => {
        this.TermOfUseOpened = true;
    }
    closeTermOfUse = () => {
        this.TermOfUseOpened = false;
    }

    openCancelationPolicy = () => {
        this.CancelationPolicyOpened = true;
    }
    closeCancelationPolicy = () => {
        this.CancelationPolicyOpened = false;
    }
    handleTermOfUse = () => {
        if(this.TermOfUseChecked){
            this.TermOfUseChecked = false;
        }
        else{
            this.TermOfUseChecked = true;
        }
    }
    handleCancelation = () => {
        if(this.CancelatonChecked){
            this.CancelatonChecked = false;
        }
        else{
            this.CancelatonChecked = true;
        }
    }
}