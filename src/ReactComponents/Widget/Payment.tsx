import React from 'react';
import styled from 'styled-components';
import WidgetStore from './WidgetStore';
import { observer } from 'mobx-react'; 
import InputMask from 'react-input-mask';
import {TermOfUse} from './TermOfUse';
import {CancelationPolicy} from './CancelationPolicy';
import {Company} from './CompanyTitle';

type Props = {
    store: WidgetStore
};

@observer
class PaymentComponent extends React.Component<Props> {
    store: WidgetStore;
    constructor(props:any){
        super(props);
        this.store = this.props.store;
    }

    render() {

        if(this.store.CancelationPolicyOpened){
            return <CancelationPolicy store={this.store}/>
        }
        if(this.store.TermOfUseOpened){
            return <TermOfUse store={this.store}/>
        }

        const riderDetails = 
        <div className="rider-details-container">
            <span className="rider-details-text">Rider details</span>
                <RiderDetailsContainer>
                    <RiderDetailsInputContainer>
                        <InputMask id="booking-payment-form-rider-firstName" style={{background: this.store.WidgetColors.BodyColor}} type="" onInput={this.store.handleRiderFirstName} className="form-area-rider-details-input" placeholder="First name*"></InputMask>
                    </RiderDetailsInputContainer>
                    <RiderDetailsInputContainer>
                        <InputMask id="booking-payment-form-rider-lastName" style={{background: this.store.WidgetColors.BodyColor}} type="" onInput={this.store.handleRiderLastName} className="form-area-rider-details-input" placeholder="Last name*"></InputMask>
                    </RiderDetailsInputContainer>
                    <RiderDetailsInputContainer>
                        <InputMask id="booking-payment-form-rider-phoneNumber" style={{background: this.store.WidgetColors.BodyColor}} type="" onInput={this.store.handleRiderPhone} className="form-area-rider-details-input" placeholder="Phone number*"></InputMask>
                    </RiderDetailsInputContainer>
                    <RiderDetailsInputContainer>
                        <InputMask id="booking-payment-form-rider-email" style={{background: this.store.WidgetColors.BodyColor}} type="" onInput={this.store.handleRiderEmail} className="form-area-rider-details-input" placeholder="Email*"></InputMask>
                    </RiderDetailsInputContainer>
                </RiderDetailsContainer>
                <span className="payment-information-text">Payment information</span>
        </div>

        return (<div>
            <MainContainer>
                <Company
                    store={this.store}/>
                <PaymentQuestionContainer>
                    <span>Payment information same as rider details?</span>
                    {this.store.RiderDetailsChecked ?
                    <AirportTransferRadio>
                        <span>No</span>
                        <input defaultChecked onClick={this.store.expandRiderDetails} className="airport-transfer-radio" name="yes_no" type="radio" ></input>
                        <span className="radio-rider-yes">Yes</span>
                        <input onClick={this.store.hideRiderDetails} className="airport-transfer-radio" name="yes_no" type="radio"></input>
                    </AirportTransferRadio> 
                    :
                    <AirportTransferRadio>
                        <span>No</span>
                        <input onClick={this.store.expandRiderDetails} className="airport-transfer-radio" name="yes_no" type="radio" ></input>
                        <span className="radio-rider-yes">Yes</span>
                        <input defaultChecked onClick={this.store.hideRiderDetails} className="airport-transfer-radio" name="yes_no" type="radio"></input>
                    </AirportTransferRadio> 
                    }             
                </PaymentQuestionContainer>
                {this.store.RiderDetailsChecked &&
                    riderDetails                  
                }
                <PaymentFormContainer>
                    <span>Full Name</span>
                    <FirstNameLastNameContainer>
                        <FirstNameContainer>
                            <InputMask id="booking-payment-form-firstName" style={{background: this.store.WidgetColors.BodyColor}} type="" value={this.store.PaymentFirstName} onInput={this.store.handleFirstName} className="form-area-airporttransfer-input" placeholder="First name"></InputMask>
                        </FirstNameContainer>
                        <LastNameContainer>
                            <InputMask id="booking-payment-form-lastName" style={{background: this.store.WidgetColors.BodyColor}} type="" value={this.store.PaymentLastName}  onInput={this.store.handleLastName} className="form-area-airporttransfer-input" placeholder="Last name"></InputMask>
                        </LastNameContainer>
                    </FirstNameLastNameContainer>
                    <EmailPhoneContainer>
                        <EmailContainer>
                            <span>Email</span>
                            <InputMask id="booking-payment-form-email" style={{background: this.store.WidgetColors.BodyColor}} type="" value={this.store.PaymentEmail}  onInput={this.store.handleEmail} className="form-area-airporttransfer-input" placeholder="username@gmail.com"></InputMask>
                        </EmailContainer>
                        <PhoneContainer>
                            <span>Phone</span>
                            <InputMask id="booking-payment-form-phoneNumber" style={{background: this.store.WidgetColors.BodyColor}} mask="+1-(999)-999-9999" type="" value={this.store.PaymentPhone}  onInput={this.store.handlePhone} className="form-area-airporttransfer-input" placeholder="+1 (415) 438-3984"></InputMask>
                        </PhoneContainer>
                    </EmailPhoneContainer>
                    <span>Address</span>
                    <AddressContainer>
                        <InputMask id="booking-payment-form-address" style={{background: this.store.WidgetColors.BodyColor}} type="" onInput={this.store.handlePaymentAddress} value={this.store.PaymentAddress} className="form-area-airporttransfer-input" placeholder="Address"></InputMask>
                    </AddressContainer>
                    <CityStateZipContainer>
                        <CityContainer>
                            <span>City</span>
                            <InputMask id="booking-payment-form-city" type="" onInput={this.store.handleCity} value={this.store.PaymentCity} className="form-area-airporttransfer-input" placeholder="San Francisco"></InputMask>
                        </CityContainer>
                        <StateContainer>
                            <span>State</span>
                            <InputMask id="booking-payment-form-state" type="" onInput={this.store.handleState} value={this.store.PaymentState} className="form-area-airporttransfer-input" placeholder="California"></InputMask>
                        </StateContainer>
                        <ZipContainer>
                            <span>ZIP</span>
                            <InputMask id="booking-payment-form-zip" type="number" onInput={this.store.handleZip} value={this.store.PaymentZip} className="form-area-airporttransfer-input" placeholder="94103"></InputMask>
                        </ZipContainer>
                    </CityStateZipContainer>
                    <span>Card</span>
                    <CardNumberContainer>
                        <InputMask id="booking-payment-form-cardNumber" style={{background: this.store.WidgetColors.BodyColor}} value={this.store.PaymentCard}  mask="9999-9999-9999-9999" onInput={this.store.handleCardNumber} type="" className="form-area-cardNumber-input" placeholder="Card number"></InputMask>
                        <InputMask style={{background: this.store.WidgetColors.BodyColor}} mask="99/99" onInput={this.store.handleCardCvcValid} value={this.store.PaymentMonthYear} type="tel" className="form-area-cardCvcValid-input" placeholder="MM/YY"></InputMask>
                        <InputMask style={{background: this.store.WidgetColors.BodyColor}} mask="999" onInput={this.store.handleCvc} type="tel" value={this.store.PaymentCVC} className="form-area-cardCvcValidNumber-input" placeholder="| CVC"></InputMask>
                    </CardNumberContainer>
                    <TermsContainer>
                        <TermsRadioContainer>
                            <label className="container-boboking-checkbox">
                                <input checked={this.store.TermOfUseChecked} onClick={this.store.handleTermOfUse} type="checkbox"/>
                                <span className="checkmark-booking-checkbox"></span>
                            </label>
                            <span onClick={this.store.openTermOfUse} className="termofuse-text">Term of use</span>
                        </TermsRadioContainer>
                        <CancelationRadioContainer>
                            <label className="container-boboking-checkbox">
                                <input checked={this.store.CancelatonChecked} onClick={this.store.handleCancelation} type="checkbox"/>
                                <span className="checkmark-booking-checkbox"></span>
                            </label>
                            <span onClick={this.store.openCancelationPolicy} className="termofuse-text">Cancelation Policy</span>
                        </CancelationRadioContainer>
                    </TermsContainer>
                    <ButtonsContainer>
                        <button style={{background: this.store.WidgetColors.SecondaryColor}} disabled={this.store.TermOfUseChecked && this.store.CancelatonChecked ? false : true} onClick={this.store.submitPaymentForm} className="pament-pay-btn">PAY {this.store.OrderAmount} $</button>
                        <button onClick={this.store.cancelAll} className="pament-cancel-btn">CANCEL</button>
                    </ButtonsContainer>
                </PaymentFormContainer>
            </MainContainer>
        </div>
        )
    }
}

export const Payment = PaymentComponent; 

//#region styled-components

const MainContainer = styled.div`
    color: white;
`;

const PaymentQuestionContainer = styled.div`
    margin-left: 10px;
    margin-top: 20px;
`;
const AirportTransferRadio = styled.div`
    display:inline-block;
    margin-left: 20%;
    line-height: 18px;
`;

const PaymentFormContainer = styled.div`
    margin: 0 auto;
    width: 89%;
    margin-top: 40px;
`;

const FirstNameLastNameContainer = styled.div`
    width: 100%;
    border-bottom: 1px solid #C4C4C4;
    padding-bottom: 5px;
`;

const FirstNameContainer = styled.div`
    display: inline-block;
    width: 50%;
`;

const LastNameContainer = styled.div`
    display: inline-block;
    width: 50%;
`;

const EmailPhoneContainer = styled.div`
    width: 100%;
    margin-top: 10px;
`;

const PhoneContainer = styled.div`
    display: inline-block;
    width: 45%;
    border-bottom: 1px solid #C4C4C4;
    padding-bottom: 5px;
    margin-left: 5%;
`;

const EmailContainer = styled.div`
    display: inline-block;
    width: 50%;
    border-bottom: 1px solid #C4C4C4;
    padding-bottom: 5px;
`;

const AddressContainer = styled.div`
    width: 100%;
    border-bottom: 1px solid #C4C4C4;
    padding-bottom: 5px;
    margin-top: 10px;
`;

const CityStateZipContainer = styled.div`
    width: 100%;
    margin-top: 10px;
    margin-bottom: 10px;
`;

const CityContainer = styled.div`
    display: inline-block;
    width: 33%;
    border-bottom: 1px solid #C4C4C4;
    padding-bottom: 5px;
`;

const StateContainer = styled.div`
    display: inline-block;
    width:  30%;
    border-bottom: 1px solid #C4C4C4;
    padding-bottom: 5px;
    margin-left: 3%;
`;

const ZipContainer = styled.div`
    display: inline-block;
    width:  30%;
    border-bottom: 1px solid #C4C4C4;
    padding-bottom: 5px;
    margin-left: 3%;
`;

const CardNumberContainer = styled.div`
    width: 100%;
    border-bottom: 1px solid #C4C4C4;
    padding-bottom: 5px;
`;

const TermsContainer = styled.div`
    display: inline-block;
    margin-top: 20px;
`;

const ButtonsContainer = styled.div`
    margin-top: 20px;
    padding-bottom: 20px;
`;

const RiderDetailsContainer = styled.div`
    margin-top: 20px;
    padding-bottom: 30px;
    margin: 0 auto;
    width: 89%;
`;

const RiderDetailsInputContainer = styled.div`
    width: 100%;
    border-bottom: 1px solid rgba(255, 255, 255, 0.12);
    padding-bottom: 5px;
    margin-top: 10px;
`;

const TermsRadioContainer = styled.div`
    display: inline-block;
`;

const CancelationRadioContainer = styled.div`
    display: inline-block;
    margin-left: 10px;
`;

const AirportTransferInput = styled.input`
	  background: none;
	  border: none;
	  outline: none;
	  color: #FFFFFF;
	  width: 100%;
	  background-color: #131126;
	  color: white;
`;

//#endregion