import React from 'react';
import styled from 'styled-components';
import WidgetStore from './WidgetStore';
import { observer } from 'mobx-react'; 
import { toJS } from 'mobx';
import Carousel from 'nuka-carousel';
import {PreloaderComp} from './Preloader';

type Props = {
    store: WidgetStore
};

@observer
class FleetChooseComponent extends React.Component<Props> {
    store: WidgetStore;
    waypoints: any;
    constructor(props: any){
        super(props);
        this.store = this.props.store;
        this.waypoints = toJS(this.store.waypoints);
    }

    componentDidMount(){

        const waypoints = toJS(this.store.waypoints);
        const startPlace = waypoints.find(x=>x.position == 0);
        const endPlace = waypoints.find(x=>x.position == 1);

        const onlyWaypoints = waypoints.filter(x=>x.position>1).sort((a, b) => (a.position > b.position) ? 1 : -1);
        const waypts = [];

        for(var i = 0; i < onlyWaypoints.length; i++){
            waypts.push({
                location: {
                    lat: onlyWaypoints[i].point.lat(),
                    lng: onlyWaypoints[i].point.lng()
                }
            });
        }

        const DirectionsService = new google.maps.DirectionsService();
                
        if(waypoints.length>1){
            DirectionsService.route({
                origin: new google.maps.LatLng(startPlace.point.lat(), startPlace.point.lng()),
                destination: new google.maps.LatLng(endPlace.point.lat(), endPlace.point.lng()),
                waypoints: waypts,
                travelMode: google.maps.TravelMode.DRIVING,
              }, (result, status) => {
                if (status === google.maps.DirectionsStatus.OK) {
                  let distance = 0;
                  let seconds = 0;
                  result.routes[0].legs.forEach(element => {
                    distance += element.distance.value;
                    seconds += element.duration.value;
                  });
                  const miles = (distance/1000) * 0.621371;
                  const minutes = seconds / 60;
                  this.store.setDistance(miles.toFixed(2));
                  this.store.setDuration(minutes.toFixed());
                  this.store.fetchCompanyCarsWithPrice();
                } else {
                  console.error(`error fetching directions ${result}`);
                }
              });
        }
    }

    render() {
        const companyCars = toJS(this.store.Cars);

        if(companyCars.length===0){
            return <PreloaderComp/>
        }

        // const cars = companyCars.filter(x=>x.carTypeId==this.store.pickedCarTypeId).map((car) =>
        const cars = companyCars.map((car) =>
        <CarDetailsContainer key={car.id}>
            <div>
            <CarImageCarouselContainer>
                <CarImageContainer>
                    <Carousel
                        renderCenterLeftControls={() => (
                            <div></div>
                        )}
                        renderCenterRightControls={() => (
                            <div></div>
                        )}>
                        {
                        car.imageUrls.map((image, i) =>
                            <img height="165px" src={image.path} key={i}/>
                        )}
                    </Carousel>
                </CarImageContainer>
            </CarImageCarouselContainer>
            {this.store.pickedCarId == car.id &&
                <CarContainer style={{
                        background: this.store.WidgetColors.SecondaryColor,
                        color: "white"
                        }} 
                        className="car-container" onClick={()=>this.store.pickFleet(car.id)}>
                    <span className="car-info-type">{car.type}</span>
                    <span className="car-info-capacity">Capacity: {car.capacity}</span>
                    <span className="car-info-make">{car.make}</span>
                    <span className="car-info-model">{car.model}</span>
                    <CarPriceContainer style={{
                        background: "white",
                        color: "black"
                        }} className="car-price-container">
                            <span className="fleet-choose-price">$ {(car.price).toFixed(2)}
                            </span></CarPriceContainer>
                </CarContainer>
            }
            {this.store.pickedCarId != car.id &&
                <CarContainer className="car-container" onClick={()=>this.store.pickFleet(car.id)}>
                    <span className="car-info-type">{car.type}</span>
                    <span className="car-info-capacity">Capacity: {car.capacity}</span>
                    <span className="car-info-make">{car.make}</span>
                    <span className="car-info-model">{car.model}</span>
                    <CarPriceContainer style={{background: this.store.WidgetColors.SecondaryColor}} className="car-price-container"><span className="fleet-choose-price">$ {(car.price).toFixed(2)}</span></CarPriceContainer>
                </CarContainer>
            }
            </div>

        </CarDetailsContainer>
        );

        return (<div>
            <MainContainer>
                <CarInfoContainer className="car-info-container">
                    <span className="company-fleet-text">Company fleet</span>
                    {cars}
                </CarInfoContainer>
                <ButtonContainer style={{pointerEvents: this.store.pickedCarId!=0? "auto" : "none"}} >
                        <button style={{background: this.store.WidgetColors.SecondaryColor}} className="next-button" onClick={this.store.nextPage}>NEXT</button>
                </ButtonContainer>
            </MainContainer>
        </div>
        )
    }
}

export const FleetPage = FleetChooseComponent;

//#region styled-components

const CarImageContainer = styled.div`
    display: inline-block;
    width: 100%; 
    height: 165px;
    background: #5A5A5A;
`;

const CarContainer = styled.div`
    display: inline-block;
    width: 64%;
    height: 165px;
    background: white;
    color: black;
    padding-top: 37px;
`;

const CarDetailsContainer = styled.div`
    width: 100%;
    height: 165px;
    margin-top: 20px;
`;

const CarInfoContainer = styled.div`
    margin: 0 auto;
    width: 90%;
    margin-top: 30px;
    height: 500px;
    overflow-y: scroll;
    overflow-x: hidden;
`;

const ButtonContainer = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 30px;
`;

const MainContainer = styled.div`
    color: white;
`;

const CarImageCarouselContainer = styled.div`
    display: inline-block;
    width: 35%; 
    height: 165px;
`;

const CarPriceContainer = styled.div`
    position: relative;
    display: block;
    font-size: 13px;
    left: 74%;
    top: -17%;
    width: 65px;
    height: 25px;
    border-radius: 4px;
    color: white;
    text-align: center;
    @media (max-width: 500px) {
        left: 66%;
  }
`;

//#endregion