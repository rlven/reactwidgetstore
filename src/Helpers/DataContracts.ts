import {ClientInfo} from '../ReactComponents/Widget/DataContracts';
export type AuthResponse = {
    token_type: string,
    access_token: string,
    expires_in: number
}

export type CompanyInfoResponse = {
    companyName: string,
    companyLogoPath: string,
    carTypes: [CaType]
    companyCars: [Car]
}

type CaType = {
    id: number,
    name: string,
    description: string,
    capacity: number
}

export type Car = {
    id: number,
    type: string,
    make: string,
    model: string,
    color: string,
    licensePlate: string,
    imageUrls: [{
        id: number,
        path: string
    }],
    price: number,
    cancelationFees: [{
        chargeStartFrom: number,
        chargeStartTo: number,
        chargePercentage: number
    }],
    capacity: number
}

export type ApiUserTokenResponse = {
    jwtToken: string,
    rtToken: string,
    identityUserId: number
}

export type CalcaulatePriceResponse = {
    orderSum: number
}

export type ReservationForm = {
    orderStartDateTime: string,
    totalOrderMileage: number,
    totalMinutes: number,
    passengersQuantity: number,
    сarInfo: Car,
    orderSum: number,
    orderNotes: string
    riderDetails: RiderDetails,
    OrderAddressDetails: OrderAddress[],
    flightNumber: string,
    isAirportPickupIncluded: boolean,
    PaymentInfo: PaymentForm,
    hours: number,
    clientInfo: ClientInfo,
    bookingType: number,
    orderType: number,
    airlines: Airlines
}

export type RiderDetails = {
    firstName: string, 
    lastName: string,
    phoneNumber: string,
    email: string
}

export type OrderAddress = {
    rideCheckPoint: string,
    pointIndexNum: number,
    latitude: number,
    longitude: number,
    placeType: number,
    placeId: string
}

export type PaymentForm = {
    CardNumber: string,
    Month: number,
    Year: number,
    Cvc: string,
    Amount: number,
    OrderId: number
}

export type CalculateRequest = {
    page: number,
    typeId: number,
    OrderAddressDetails: OrderAddress[],
    isGateMeeting: boolean
}

export type Airlines = {
    id: number,
    name: string,
    code: string
}