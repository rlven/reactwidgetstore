import axios from 'axios';
import { ApiUserTokenResponse, CompanyInfoResponse, ReservationForm, PaymentForm, Car, CalculateRequest, Airlines } from './DataContracts';

const apiUrl = 'http://bookinglane-api.us-east-2.elasticbeanstalk.com/api';

export default class RequestHelper{

    static authByAccessKey = (accessKey: string): Promise<ApiUserTokenResponse> => {
      const company0Key = "35c0b3e4-50b4-4b02-8ea6-237811b6cebd";
      const usLimoKey = "cae2dcc7-94af-49b8-94f9-db927a055898";
        return axios.post(apiUrl + '/companywidget/company-widget-auth?accessKey='+company0Key)
             .then(response => response.data as ApiUserTokenResponse);
    }

    static getCompanyInfo = (token: string): Promise<CompanyInfoResponse> => {

      const headers = {
          'Authorization': 'Bearer ' + token,
          "Access-Control-Allow-Origin": "*"
        };

      return axios.get(apiUrl + '/companywidget/company-widget-info', {headers: headers})
           .then(response => response.data as CompanyInfoResponse);
  }

    static calculateMileagePrice = (token: string, body: CalculateRequest): Promise<Car[]> => {

      const headers = {
          'Authorization': 'Bearer ' + token,
          "Access-Control-Allow-Origin": "*"
        };

      return axios.post(apiUrl + '/car/mileageCars-withPrice', body, {headers: headers})
          .then(response => response.data as Car[]);
  }

    static calculateHourlyPrice = (token: string, hours: number, typeId: number): Promise<Car[]> => {

      const headers = {
          'Authorization': 'Bearer ' + token,
          "Access-Control-Allow-Origin": "*"
        };

      return axios.get(apiUrl + '/car/hourlyCars-withPrice?hours='+hours+"&page=1"+"&typeId="+typeId, {headers: headers})
          .then(response => response.data as Car[]);
  }

    static calculateAirportInclPrice = (token: string, body: CalculateRequest): Promise<Car[]> => {

        const headers = {
          'Authorization': 'Bearer ' + token,
          "Access-Control-Allow-Origin": "*"
        };

      return axios.post(apiUrl + '/car/airportCars-withPrice',body, {headers: headers})
            .then(response => response.data as Car[]);
      }

    // static createReservationByMileageOrder = (form: ReservationForm) => {
    //   var token = window.localStorage.getItem("AuthToken");
    //   token = JSON.parse(token);
    //   const headers = {
    //       'Authorization': 'Bearer ' + token.jwtToken,
    //       "Access-Control-Allow-Origin": "*"
    //   };

    //   return axios.post(apiUrl + '/reservation/mileage', form, {headers: headers})
    //   .then(response => response.data as number);
    //   }

      // static createReservationHourlyOrder = (form: ReservationForm) => {
      //   var token = window.localStorage.getItem("AuthToken");
      //   token = JSON.parse(token);
      //   const headers = {
      //       'Authorization': 'Bearer ' + token.jwtToken,
      //       "Access-Control-Allow-Origin": "*"
      //   };
  
      //   return axios.post(apiUrl + '/reservation/hour', form, {headers: headers})
      //   .then(response => response.data as number);
      //   }

        // static createReservationAirport = (form: ReservationForm) => {
        //   var token = window.localStorage.getItem("AuthToken");
        //   token = JSON.parse(token);
        //   const headers = {
        //       'Authorization': 'Bearer ' + token.jwtToken,
        //       "Access-Control-Allow-Origin": "*"
        //   };
    
        //   return axios.post(apiUrl + '/reservation/transfer', form, {headers: headers})
        //   .then(response => response.data as number);
        //   }

          static createReservation = (form: ReservationForm) => {
            var token = window.localStorage.getItem("AuthToken");
            token = JSON.parse(token);
            const headers = {
                'Authorization': 'Bearer ' + token.jwtToken,
                "Access-Control-Allow-Origin": "*"
            };
      
            return axios.post(apiUrl + '/reservation', form, {headers: headers})
            .then(response => response.data as number);
            }
  

      static makePayment = (form: PaymentForm) => {
          var token = window.localStorage.getItem("AuthToken");
          token = JSON.parse(token);
          const headers = {
              'Authorization': 'Bearer ' + token.jwtToken,
              "Access-Control-Allow-Origin": "*"
          };
    
          return axios.post(apiUrl + '/payment/pay-reservation', form, {headers: headers})
          .then(response => response.data);
        };

        static getAirlines = () => {
          var token = window.localStorage.getItem("AuthToken");
          token = JSON.parse(token);
          const headers = {
              'Authorization': 'Bearer ' + token.jwtToken,
              "Access-Control-Allow-Origin": "*"
          };
    
          return axios.get(apiUrl + '/place/airlines', {headers: headers})
          .then(response => response.data as Airlines[]);
        };
}

