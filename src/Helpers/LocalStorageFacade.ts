import  Helpers  from './Helpers';

type LocalStorageMap = {

};

export class LocalStorageFacade {

    public static getValue<T extends keyof LocalStorageMap>(key: any): LocalStorageMap[T] | null {

        if (!Helpers.isClient()) {
            throw 'Never: client only code';
        }

        const valueAsString = window.localStorage.getItem(key);
        return valueAsString === null ? null
            : JSON.parse(valueAsString) as LocalStorageMap[T];
    }

    public static setValue <T extends keyof LocalStorageMap> (key: any, value: LocalStorageMap[T]) : void {

        if (!Helpers.isClient()) {
            throw 'Never: client only code';
        }

        window.localStorage.setItem(key, JSON.stringify(value));
    }

    public static deleteValue <T extends keyof LocalStorageMap> (key: T) : void {

        if (!Helpers.isClient()) {
            throw 'Never: client only code';
        }

        window.localStorage.removeItem(key);
    }
}