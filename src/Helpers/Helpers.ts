import getRandomValues from 'get-random-values';
import * as React from 'react';

export default class Helpers {

    static anchorClick(func: () => any): (e: React.MouseEvent<HTMLAnchorElement>) => void {
        return e => {
            e.preventDefault();
            func();
        };
    }

    static onControlKeys(handlers: { esc?: () => void, enter?: () => void }) {
        return (e: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>) => {
            const isEnter = e.keyCode === 13;
            if (isEnter && handlers.enter) {
                handlers.enter();
            }

            const isEsc = e.keyCode === 27;
            if (isEsc && handlers.esc) {
                handlers.esc();
            }
        };
    }

    static inputChanged(func: (value: string) => any): React.FormEventHandler<{}> {
        return e => {
            func((e.target as any as { value: string }).value);
        };
    }

    static isClient = () => {
        return typeof document !== 'undefined' && document.createElement;
    }

    static getCurrentDate = () => {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var tt = String(today.getHours()).padStart(2, '0');;
        var min = String(today.getMinutes()).padStart(2, '0');;

        return yyyy + '-' + mm + '-' + dd+'T'+tt+':'+min;
    }

    static generateGuid = () =>
        ((1e7).toString() + -1e3 + -4e3 + -8e3 + -1e11)
        .replace(/[018]/g, c => (Number(c) ^ getRandomValues(new Uint8Array(1))[0] & 15 >> Number(c) / 4).toString(16))

    static range = (length: number, start?: number) => Array.from(new Array(length), (_, i) => i + (start || 0));
}