export const errorIds: string[] = [
  "booking-location-from-name",
  "booking-location-to-name",
  "booking-widget-time-inputName",
  "boooking-widget-people-count",
  "booking-widget-hourly-inputName",
  "booking-widget-airportTransfer-inputName",
  "booking-widget-cartype-inputName",
  "booking-widget-hour-count-input",
  "booking-widget-airlines-info-input",
  "booking-widget-airlines-gateMeeting-input"
];

export const validate = (values: any, isHourly: boolean, isAirportInc: boolean) => {
    let errors: string[] = [];
  
    if (values.locationFromName ==='') {
      errors.push("booking-location-from-name");
    } 

    if (values.locationToName ==='') {
      errors.push("booking-location-to-name");
    } 

    if(values.date === ''){
      errors.push("booking-widget-time-inputName");
    }

    if(values.peopleCount < 1) {
      errors.push("boooking-widget-people-count");
    }

    if(values.isHourly === ''){
      errors.push("booking-widget-hourly-inputName");
    }

    if(values.airportTransfer === ''){
      errors.push("booking-widget-airportTransfer-inputName");
    }

    if(values.pickedCarTypeId === 0){
      errors.push("booking-widget-cartype-inputName");
    }

    if(isHourly && values.hoursCount < 1){
      errors.push("booking-widget-hour-count-input");
    }

    if(isAirportInc && values.airportTransferInfo){
      if(values.airportTransferInfo.Airlines === ''){
        errors.push("booking-widget-airlines-info-input");
      }
      if(values.airportTransferInfo.GateMeeting === ''){
        errors.push("booking-widget-airlines-gateMeeting-input");
      }
    }

    return errors;
  };