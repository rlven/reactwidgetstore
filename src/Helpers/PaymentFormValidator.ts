import { RiderDetails } from "./DataContracts";

export const paymentErrorIds: string[] = [
    "booking-payment-form-firstName",
    "booking-payment-form-lastName",
    "booking-payment-form-email",
    "booking-payment-form-phoneNumber",
    "booking-payment-form-address",
    "booking-payment-form-city",
    "booking-payment-form-state",
    "booking-payment-form-zip",
    "booking-payment-form-cardNumber",
    "booking-payment-form-cardInfo",
    "booking-payment-form-rider-firstName",
    "booking-payment-form-rider-lastName",
    "booking-payment-form-rider-email",
    "booking-payment-form-rider-phoneNumber"
  ];
  
  export const validatePaymentForm = (values: any, isRiderDetails: boolean, riderDetails: RiderDetails) => {
      let errors: string[] = [];
    
      if (values.firstName ==='') {
        errors.push("booking-payment-form-firstName");
      } 
  
      if (values.lastName ==='') {
        errors.push("booking-payment-form-lastName");
      } 
  
      if(values.email === ''){
        errors.push("booking-payment-form-email");
      }
  
      if(values.phoneNumber === '') {
        errors.push("booking-payment-form-phoneNumber");
      }
  
      if(values.address === ''){
        errors.push("booking-payment-form-address");
      }
  
      if(values.city === ''){
        errors.push("booking-payment-form-city");
      }
  
      if(values.state === ''){
        errors.push("booking-payment-form-state");
      }
  
      if(values.zip === ''){
        errors.push("booking-payment-form-zip");
      }

      if(values.cardNumber === ''){
        errors.push("booking-payment-form-cardNumber");
      }

      if(values.cardNumber === ''){
        errors.push("booking-payment-form-cardInfo");
      }

      if(!isRiderDetails){
        if(riderDetails.firstName === ''){
            errors.push("booking-payment-form-rider-firstName");
          }
          if(riderDetails.lastName === ''){
            errors.push("booking-payment-form-rider-lastName");
          }
          if(riderDetails.email === ''){
            errors.push("booking-payment-form-rider-email");
          }
          if(riderDetails.phoneNumber === ''){
            errors.push("booking-payment-form-rider-phoneNumber");
          }
      }
  
      return errors;
    };